package pl.bbsw.sport.club.event;

public class EventId {
    public final long id;

    public EventId(long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return ((Long)id).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof EventId && ((EventId)o).id == id;
    }

    @Override
    public String toString() {
        return ((Long)id).toString();
    }

    public static EventId invalid = new EventId(0);
}
