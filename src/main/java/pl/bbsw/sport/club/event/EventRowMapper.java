package pl.bbsw.sport.club.event;

import org.springframework.jdbc.core.RowMapper;
import pl.bbsw.sport.club.club_localization.ClubLocalizationRowMapper;

public class EventRowMapper {
    public static final RowMapper<EventId> eventId = (rs, rowId) -> new EventId(rs.getLong("evt_id"));

    public static final RowMapper<Event> event = (rs, rowId) -> new Event(
            eventId.mapRow(rs, rowId),
            ClubLocalizationRowMapper.clubLocalization.mapRow(rs, rowId),
            EventType.withId(rs.getString("evt_type")),
            rs.getString("evt_name"),
            rs.getString("evt_description"),
            rs.getDate("evt_start_date"),
            rs.getDate("evt_end_date")
    );
}
