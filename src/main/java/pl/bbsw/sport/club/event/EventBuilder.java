package pl.bbsw.sport.club.event;

import pl.bbsw.sport.club.club_localization.ClubLocalization;

import java.util.Date;

public class EventBuilder {
    private EventId maybeId;
    private ClubLocalization maybeAddress;
    private EventType maybeType;
    private String maybeName;
    private String maybeDescription;
    private Date maybeStartDate;
    private Date maybeEndDate;

    public EventBuilder(Event event) {
        this.maybeId = event.getId();
        this.maybeAddress = event.getAddress();
        this.maybeType = event.getType();
        this.maybeName = event.getName();
        this.maybeDescription = event.getDescription();
        this.maybeStartDate = event.getStartDate();
        this.maybeEndDate = event.getEndDate();
    }

    public EventBuilder withId(EventId id) {
        this.maybeId = id;
        return this;
    }

    public EventBuilder withAddress(ClubLocalization address) {
        this.maybeAddress = address;
        return this;
    }

    public EventBuilder withType(EventType type) {
        this.maybeType = type;
        return this;
    }

    public EventBuilder withName(String name) {
        this.maybeName = name;
        return this;
    }

    public EventBuilder withDescription(String description) {
        this.maybeDescription = description;
        return this;
    }

    public EventBuilder withStartDate(Date startDate) {
        this.maybeStartDate = startDate;
        return this;
    }

    public EventBuilder withEndDate(Date endDate) {
        this.maybeEndDate = endDate;
        return this;
    }

    public Event build() {
        return new Event(maybeId, maybeAddress, maybeType, maybeName, maybeDescription, maybeStartDate, maybeEndDate);
    }
}
