package pl.bbsw.sport.club.event;

import pl.bbsw.sport.club.club_localization.ClubLocalization;
import java.util.Date;

public class Event {
    private final EventId id;
    private final ClubLocalization address;
    private final EventType type;
    private final String name;
    private final String description;
    private final Date startDate;
    private final Date endDate;

    public Event(EventId id, ClubLocalization address, EventType type, String name, String description, Date startDate, Date endDate) {
        this.id = id;
        this.address = address;
        this.type = type;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public EventId getId() {
        return id;
    }

    public ClubLocalization getAddress() {
        return address;
    }

    public EventType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
