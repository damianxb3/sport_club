package pl.bbsw.sport.club.event;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.bbsw.sport.club.event.dto.EventDto;
import pl.bbsw.sport.club.event.dto.NewEventDto;
import pl.bbsw.sport.club.util.Pagination;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/event")
public class EventController {
    private EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PutMapping("/create")
    public EventId create(@RequestBody NewEventDto eventData) {
        return eventService.create(eventData);
    }

    @GetMapping("/list/{page}/{pageSize}")
    public List<Event> list(
            @Min(1) @PathVariable int page,
            @Min(1) @Max(Pagination.MAX_PAGE_SIZE) @PathVariable int pageSize
    ) {
        return eventService.list(page, pageSize);
    }

    @GetMapping("/count")
    public long count() {
        return eventService.count();
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody EventDto eventData) {
        eventService.update(eventData);
        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@Min(1) @PathVariable long id) {
        eventService.delete(id);
        return ResponseEntity.ok(null);
    }

    @GetMapping("/get/{id}")
    public Event get(@Min(1) @PathVariable long id) {
        return eventService.get(id).orElse(null);
    }

    @GetMapping("/all")
    public List<Event> listAll() {
        return eventService.listAll();
    }
}