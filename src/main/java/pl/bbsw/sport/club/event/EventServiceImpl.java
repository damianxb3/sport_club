package pl.bbsw.sport.club.event;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.bbsw.sport.club.club_localization.ClubLocalizationService;
import pl.bbsw.sport.club.event.dto.EventDto;
import pl.bbsw.sport.club.event.dto.NewEventDto;

import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {
    private EventDao eventDao;
    private ClubLocalizationService clubLocalizationService;

    public EventServiceImpl(EventDao eventDao, ClubLocalizationService clubLocalizationService) {
        this.eventDao = eventDao;
        this.clubLocalizationService = clubLocalizationService;
    }

    @Override
    @Transactional
    public EventId create(NewEventDto eventData) {
        return eventDao.insert(eventData);
    }

    @Override
    @Transactional
    public void update(EventDto eventData) {
        eventDao.update(eventData);
    }

    @Override
    @Transactional
    public void delete(long id) {
        eventDao.delete(new EventId(id));
    }

    @Override
    public List<Event> list(int page, int pageSize) {
        return eventDao.list(page, pageSize);
    }

    @Override
    public long count() {
        return eventDao.count();
    }

    @Override
    public Optional<Event> get(long id) {
        return eventDao.get(new EventId(id));
    }

    @Override
    public List<Event> listAll() {
        return eventDao.listAll();
    }
}
