package pl.bbsw.sport.club.event;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import pl.bbsw.sport.club.event.dto.EventDto;
import pl.bbsw.sport.club.event.dto.NewEventDto;

import java.util.List;
import java.util.Optional;

@Repository
public class EventDaoImpl implements EventDao {
    private NamedParameterJdbcTemplate jdbcTemplate;

    public EventDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public EventId insert(NewEventDto eventData) {
        final String sql = "INSERT INTO event(evt_address_id, evt_type, evt_name, evt_description," +
                "evt_start_date, evt_end_date) VALUES (:addressId, :type, :name, :description, :startDate, :endDate)";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("addressId", eventData.getAddressId())
                .addValue("type", EventType.withId(eventData.getType()).id)
                .addValue("name", eventData.getName())
                .addValue("description", eventData.getDescription())
                .addValue("startDate", eventData.getStartDate())
                .addValue("endDate", eventData.getEndDate());

        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, params, keyHolder, new String[] {"evt_id"});

        return new EventId(keyHolder.getKey().longValue());
    }

    @Override
    public void update(EventDto eventData) {
        final String sql = "UPDATE event SET evt_address_id = :addressId, evt_type = :type, evt_name = :name," +
                "evt_description = :description, evt_start_date = :startDate," +
                "evt_end_date = :endDate WHERE evt_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", eventData.getId().getId())
                .addValue("addressId", eventData.getAddressId())
                .addValue("type", eventData.getType().id)
                .addValue("name", eventData.getName())
                .addValue("description", eventData.getDescription())
                .addValue("startDate", eventData.getStartDate())
                .addValue("endDate", eventData.getEndDate());

        final int count = jdbcTemplate.update(sql, params);
        if (count != 1)
            throw new IllegalStateException("Invalid number of columns updated: " + count);
    }

    @Override
    public void delete(EventId eventId) {
        final String sql = "DELETE FROM event WHERE evt_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", eventId.id);

        final int count = jdbcTemplate.update(sql, params);
        if (count != 1)
            throw new IllegalStateException("Invalid number of rows deleted: " + count);
    }

    @Override
    public Optional<Event> get(EventId id) {
        final String sql = "SELECT * FROM event evt " +
                "JOIN club_localization cll " +
                "ON evt.evt_address_id = cll.ads_id " +
                "WHERE evt.evt_id = :id";

        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id.id);

        return jdbcTemplate.query(sql, params, EventRowMapper.event)
                .stream()
                .findFirst();
    }

    @Override
    public List<Event> list(int page, int pageSize) {
        final String sql = "SELECT * FROM event evt " +
                "JOIN club_localization cll " +
                "ON evt.evt_address_id = cll.ads_id " +
                "ORDER BY evt.evt_id LIMIT :count OFFSET :offset";

        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("count", pageSize)
                .addValue("offset", (page - 1) * pageSize);

        return jdbcTemplate.query(sql, params, EventRowMapper.event);
    }

    @Override
    public long count() {
        final String sql = "SELECT count(1) FROM event";
        return jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong(1)).get(0);
    }

    @Override
    public List<Event> listAll() {
        final String sql = "SELECT * FROM event evt " +
                "JOIN club_localization cll " +
                "ON evt.evt_address_id = cll.ads_id " +
                "ORDER BY evt.evt_id";
        return jdbcTemplate.query(sql, EventRowMapper.event);
    }
}
