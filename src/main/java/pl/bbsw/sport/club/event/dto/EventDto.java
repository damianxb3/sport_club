package pl.bbsw.sport.club.event.dto;

import pl.bbsw.sport.club.event.EventType;

import java.util.Date;

public class EventDto {
    private EventIdDto id;
    private Long addressId;
    private EventType type;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;

    public EventIdDto getId() {
        return id;
    }

    public void setId(EventIdDto id) {
        this.id = id;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
