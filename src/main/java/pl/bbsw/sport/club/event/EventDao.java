package pl.bbsw.sport.club.event;

import pl.bbsw.sport.club.event.dto.EventDto;
import pl.bbsw.sport.club.event.dto.NewEventDto;

import java.util.List;
import java.util.Optional;

public interface EventDao {
    EventId insert(NewEventDto event);

    void update(EventDto section);

    void delete(EventId sectionId);

    Optional<Event> get(EventId id);

    List<Event> list(int page, int pageSize);

    long count();

    List<Event> listAll();
}
