package pl.bbsw.sport.club.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;

public enum EventType {
    Competition("Competition"),
    Camp("Camp"),
    Gathering("Gathering");

    public final String id;
    EventType(String id) {
        this.id = id;
    }

    @JsonCreator
    public static EventType withId(String id) {
        final Optional<EventType> result = Arrays.stream(values())
                .filter(v -> v.id.equals(id))
                .findFirst();
        if (!result.isPresent())
            throw new NoSuchElementException();
        return result.get();
    }


    @Override
    @JsonValue
    public String toString() {
        return id;
    }
}
