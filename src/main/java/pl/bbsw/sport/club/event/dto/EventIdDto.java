package pl.bbsw.sport.club.event.dto;

import pl.bbsw.sport.club.event.EventId;

public class EventIdDto {
    private long id;

    public EventId toEntity() {
        return new EventId(id);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
