package pl.bbsw.sport.club.event;

import pl.bbsw.sport.club.event.dto.EventDto;
import pl.bbsw.sport.club.event.dto.NewEventDto;

import java.util.List;
import java.util.Optional;

public interface EventService {
    EventId create(NewEventDto event);

    void update(EventDto event);

    void delete(long id);

    List<Event> list(int page, int pageSize);

    long count();

    Optional<Event> get(long id);

    List<Event> listAll();
}
