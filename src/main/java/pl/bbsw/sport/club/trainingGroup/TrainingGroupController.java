package pl.bbsw.sport.club.trainingGroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.bbsw.sport.club.trainingGroup.dto.NewTrainingGroupDto;
import pl.bbsw.sport.club.trainingGroup.dto.TrainingGroupUpdateDto;
import pl.bbsw.sport.club.util.Pagination;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/trainingGroup")
@Validated
public class TrainingGroupController {
    private TrainingGroupService trainingGroupService;

    @Autowired
    public TrainingGroupController(TrainingGroupService trainingGroupService) {
        this.trainingGroupService = trainingGroupService;
    }

    @PutMapping("/create")
    @ResponseBody
    public TrainingGroupId create(@RequestBody NewTrainingGroupDto newTrainingGroupDto) {
        return trainingGroupService.create(newTrainingGroupDto);
    }

    @GetMapping("/list/{page}/{pageSize}")
    @ResponseBody
    public List<TrainingGroup> list(
            @Min(1) @PathVariable int page,
            @Min(1) @Max(Pagination.MAX_PAGE_SIZE) @PathVariable int pageSize
    ) {
        return trainingGroupService.list(page, pageSize);
    }

    @GetMapping("/count")
    @ResponseBody
    public long count() {
        return trainingGroupService.count();
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody TrainingGroupUpdateDto updateDto) {
        trainingGroupService.update(updateDto);
        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/delete/{trainingGroupId}")
    public ResponseEntity<?> delete(@Min(1) @PathVariable long trainingGroupId) {
        trainingGroupService.delete(new TrainingGroupId(trainingGroupId));
        return ResponseEntity.ok(null);
    }

    @GetMapping("/get/{trainingGroupId}")
    @ResponseBody
    public TrainingGroup get(@Min(1) @PathVariable long trainingGroupId) {
        return trainingGroupService.get(new TrainingGroupId(trainingGroupId)).orElse(null);
    }

    @GetMapping("/all")
    @ResponseBody
    public List<TrainingGroup> listAll() {
        return trainingGroupService.listAll();
    }
}
