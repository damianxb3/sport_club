package pl.bbsw.sport.club.trainingGroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bbsw.sport.club.trainingGroup.dto.NewTrainingGroupDto;
import pl.bbsw.sport.club.trainingGroup.dto.TrainingGroupUpdateDto;

import java.util.List;
import java.util.Optional;

@Service
public class TrainingGroupServiceImpl implements TrainingGroupService {
    private TrainingGroupDao trainingGroupDao;

    @Autowired
    public TrainingGroupServiceImpl(TrainingGroupDao trainingGroupDao) {
        this.trainingGroupDao = trainingGroupDao;
    }

    @Override
    public TrainingGroupId create(NewTrainingGroupDto trainingGroup) {
        return trainingGroupDao.insert(trainingGroup);
    }

    @Override
    public void update(TrainingGroupUpdateDto trainingGroup) {
        trainingGroupDao.update(trainingGroup);
    }

    @Override
    public void delete(TrainingGroupId trainingGroupId) {
        trainingGroupDao.delete(trainingGroupId);
    }

    @Override
    public List<TrainingGroup> list(int page, int pageSize) {
        return trainingGroupDao.list(page, pageSize);
    }

    @Override
    public long count() {
        return trainingGroupDao.count();
    }

    @Override
    public Optional<TrainingGroup> get(TrainingGroupId id) {
        return trainingGroupDao.get(id);
    }

    @Override
    public List<TrainingGroup> listAll() {
        return trainingGroupDao.listAll();
    }
}
