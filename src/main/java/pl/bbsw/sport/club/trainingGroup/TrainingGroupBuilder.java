package pl.bbsw.sport.club.trainingGroup;

import com.google.common.base.Preconditions;
import pl.bbsw.sport.club.section.Section;

import java.math.BigDecimal;

import static com.google.common.base.Preconditions.checkNotNull;

public class TrainingGroupBuilder {
    private TrainingGroupId id;
    private Section section;
    private BigDecimal monthlyFee;

    public TrainingGroupBuilder() {
    }

    public TrainingGroupBuilder(TrainingGroup trainingGroup) {
        checkNotNull(trainingGroup);

        id = trainingGroup.getId();
        section = trainingGroup.getSection();
        monthlyFee = trainingGroup.getMonthlyFee();
    }

    public TrainingGroupBuilder withId(TrainingGroupId id) {
        this.id = id;
        return this;
    }

    public TrainingGroupBuilder withSection(Section section) {
        this.section = section;
        return this;
    }

    public TrainingGroupBuilder withMonthlyFee(BigDecimal monthlyFee) {
        this.monthlyFee = monthlyFee;
        return this;
    }

    public TrainingGroup build() {
        checkNotNull(id);
        checkNotNull(section);
        checkNotNull(monthlyFee);
        return new TrainingGroup(id, section, monthlyFee);
    }
}
