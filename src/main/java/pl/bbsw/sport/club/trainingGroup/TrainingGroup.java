package pl.bbsw.sport.club.trainingGroup;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import pl.bbsw.sport.club.section.Section;

import java.math.BigDecimal;

import static com.google.common.base.Preconditions.checkNotNull;

public class TrainingGroup {
    private final TrainingGroupId id;
    private final Section section;
    private final BigDecimal monthlyFee;

    public TrainingGroup(TrainingGroupId id, Section section, BigDecimal monthlyFee) {
        checkNotNull(id);
        checkNotNull(section);
        checkNotNull(monthlyFee);

        this.id = id;
        this.section = section;
        this.monthlyFee = monthlyFee;
    }

    public TrainingGroupId getId() {
        return id;
    }

    public Section getSection() {
        return section;
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrainingGroup that = (TrainingGroup) o;
        return Objects.equal(id, that.id) &&
                Objects.equal(section, that.section) &&
                Objects.equal(monthlyFee, that.monthlyFee);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, section, monthlyFee);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("section", section)
                .add("monthlyFee", monthlyFee)
                .toString();
    }
}
