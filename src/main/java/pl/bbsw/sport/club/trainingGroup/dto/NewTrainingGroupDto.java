package pl.bbsw.sport.club.trainingGroup.dto;

import pl.bbsw.sport.club.trainingGroup.TrainingGroup;
import pl.bbsw.sport.club.trainingGroup.TrainingGroupId;

import java.math.BigDecimal;

public class NewTrainingGroupDto {
    private Long sectionId;
    private BigDecimal monthlyFee;

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(BigDecimal monthlyFee) {
        this.monthlyFee = monthlyFee;
    }
}
