package pl.bbsw.sport.club.trainingGroup.dto;

public class TrainingGroupIdDto {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
