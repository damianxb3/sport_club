package pl.bbsw.sport.club.trainingGroup;

import pl.bbsw.sport.club.trainingGroup.dto.NewTrainingGroupDto;
import pl.bbsw.sport.club.trainingGroup.dto.TrainingGroupUpdateDto;

import java.util.List;
import java.util.Optional;

public interface TrainingGroupDao {
    TrainingGroupId insert(NewTrainingGroupDto trainingGroup);
    
    void update(TrainingGroupUpdateDto trainingGroup);
    
    void delete(TrainingGroupId id);
    
    Optional<TrainingGroup> get(TrainingGroupId id);
    
    List<TrainingGroup> list(int page, int pageSize);
    
    long count();

    List<TrainingGroup> listAll();
}
