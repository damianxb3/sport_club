package pl.bbsw.sport.club.trainingGroup.dto;

import java.math.BigDecimal;

public class TrainingGroupUpdateDto {
    private TrainingGroupIdDto id;
    private Long sectionId;
    private BigDecimal monthlyFee;

    public TrainingGroupIdDto getId() {
        return id;
    }

    public void setId(TrainingGroupIdDto id) {
        this.id = id;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(BigDecimal monthlyFee) {
        this.monthlyFee = monthlyFee;
    }
}
