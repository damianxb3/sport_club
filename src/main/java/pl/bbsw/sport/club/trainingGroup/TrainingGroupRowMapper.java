package pl.bbsw.sport.club.trainingGroup;

import org.springframework.jdbc.core.RowMapper;
import pl.bbsw.sport.club.section.SectionRowMapper;

import java.math.BigDecimal;

public class TrainingGroupRowMapper {
    public static final RowMapper<TrainingGroupId> trainingGroupId =
            (rs, rowId) -> new TrainingGroupId(rs.getLong("trg_id"));

    public static final RowMapper<TrainingGroup> trainingGroup = (rs, rowId) -> new TrainingGroup(
            trainingGroupId.mapRow(rs, rowId),
            SectionRowMapper.section.mapRow(rs, rowId),
            rs.getBigDecimal("trg_monthly_fee")
    );
}
