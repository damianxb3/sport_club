package pl.bbsw.sport.club.trainingGroup;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class TrainingGroupId {
    public final long id;

    public TrainingGroupId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrainingGroupId that = (TrainingGroupId) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .toString();
    }

    public static final TrainingGroupId invalid = new TrainingGroupId(0);
}
