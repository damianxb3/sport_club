package pl.bbsw.sport.club.trainingGroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import pl.bbsw.sport.club.trainingGroup.dto.NewTrainingGroupDto;
import pl.bbsw.sport.club.trainingGroup.dto.TrainingGroupUpdateDto;

import java.util.List;
import java.util.Optional;

@Repository
public class TrainingGroupDaoImpl implements TrainingGroupDao {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public TrainingGroupDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public TrainingGroupId insert(NewTrainingGroupDto trainingGroup) {
        final String sql = "INSERT INTO training_group(trg_sport_section_id, trg_monthly_fee) " +
                "values(:sport_section_id, :monthly_fee)";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("sport_section_id", trainingGroup.getSectionId())
                .addValue("monthly_fee", trainingGroup.getMonthlyFee());

        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, params, keyHolder, new String[] {"trg_id"});

        return new TrainingGroupId(keyHolder.getKey().longValue());
    }

    @Override
    public void update(TrainingGroupUpdateDto trainingGroup) {
        final String sql = "UPDATE training_group SET trg_sport_section_id = :sport_section_id," +
                "trg_monthly_fee = :monthly_fee WHERE trg_id = :id";

        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", trainingGroup.getId().getId())
                .addValue("sport_section_id", trainingGroup.getSectionId())
                .addValue("monthly_fee", trainingGroup.getMonthlyFee());

        jdbcTemplate.update(sql, params);
    }

    @Override
    public void delete(TrainingGroupId id) {
        final String sql = "DELETE FROM training_group WHERE trg_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id.id);
        jdbcTemplate.update(sql, params);
    }

    @Override
    public Optional<TrainingGroup> get(TrainingGroupId id) {
        final String sql = "SELECT * FROM training_group " +
                "JOIN sport_section ON sps_id = trg_sport_section_id WHERE trg_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id.id);

        return jdbcTemplate.query(sql, params, TrainingGroupRowMapper.trainingGroup).stream().findFirst();
    }

    @Override
    public List<TrainingGroup> list(int page, int pageSize) {
        final String sql = "SELECT * FROM training_group " +
                "JOIN sport_section ON sps_id = trg_sport_section_id OFFSET :offset LIMIT :pageSize";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("offset", (page - 1) * pageSize)
                .addValue("pageSize", pageSize);

        return jdbcTemplate.query(sql, params, TrainingGroupRowMapper.trainingGroup);
    }

    @Override
    public long count() {
        final String sql = "SELECT count(1) FROM training_group";
        return jdbcTemplate.query(sql, (rs, rowid) -> rs.getLong(1)).get(0);
    }

    @Override
    public List<TrainingGroup> listAll() {
        final String sql = "SELECT * FROM training_group JOIN sport_section ON sps_id = trg_sport_section_id";
        return jdbcTemplate.query(sql, TrainingGroupRowMapper.trainingGroup);
    }
}
