package pl.bbsw.sport.club.club_localization;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.bbsw.sport.club.club_localization.dto.ClubLocalizationDto;
import pl.bbsw.sport.club.club_localization.dto.NewClubLocalizationDto;
import pl.bbsw.sport.club.util.Pagination;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/address")
@Validated
public class ClubLocalizationController {
    private ClubLocalizationService clubLocalizationService;

    public ClubLocalizationController(ClubLocalizationService clubLocalizationService) {
        this.clubLocalizationService = clubLocalizationService;
    }

    @PutMapping("/create")
    public ClubLocalizationId create(@RequestBody NewClubLocalizationDto newClubLocalizationDto) {
        return clubLocalizationService.create(newClubLocalizationDto);
    }

    @GetMapping("/list/{page}/{pageSize}")
    public List<ClubLocalization> list(
            @Min(1) @PathVariable int page,
            @Min(1) @Max(Pagination.MAX_PAGE_SIZE) @PathVariable int pageSize
    ) {
        return clubLocalizationService.list(page, pageSize);
    }

    @GetMapping("/count")
    public long count() {
        return clubLocalizationService.count();
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody ClubLocalizationDto clubLocalization) {
        clubLocalizationService.update(clubLocalization);
        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@Min(1) @PathVariable long id) {
        clubLocalizationService.delete(id);
        return ResponseEntity.ok(null);
    }

    @GetMapping("/get/{id}")
    public ClubLocalization get(@Min(1) @PathVariable long id) {
        return clubLocalizationService.get(id).orElse(null);
    }

    @GetMapping("/all")
    public List<ClubLocalization> listAll() {
        return clubLocalizationService.listAll();
    }
}
