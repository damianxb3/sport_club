package pl.bbsw.sport.club.club_localization;

public class ClubLocalization {
    private final ClubLocalizationId id;
    private final String city;
    private final String street;
    private final String number;
    private final String postalCode;
    private final String phone;

    public ClubLocalization(ClubLocalizationId id, String city, String street, String number, String postalCode, String phone) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.number = number;
        this.postalCode = postalCode;
        this.phone = phone;
    }

    public ClubLocalizationId getId() {
        return id;
    }

    public String getCity() {
        return city;
    }


    public String getStreet() {
        return street;
    }


    public String getNumber() {
        return number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getPhone() {
        return phone;
    }
}
