package pl.bbsw.sport.club.club_localization.dto;

import pl.bbsw.sport.club.club_localization.ClubLocalization;
import pl.bbsw.sport.club.club_localization.ClubLocalizationId;

public class NewClubLocalizationDto {
    private String city;
    private String street;
    private String number;
    private String postalCode;
    private String phone;

    public ClubLocalization toEntity() {
        return new ClubLocalization(ClubLocalizationId.invalid, city, street, number, postalCode, phone);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
