package pl.bbsw.sport.club.club_localization;

import java.util.List;
import java.util.Optional;

public interface ClubLocalizationDao {
    ClubLocalization insert(ClubLocalization clubLocalization);

    void update(ClubLocalization clubLocalization);

    void delete(ClubLocalizationId clubLocalizationId);

    Optional<ClubLocalization> get(ClubLocalizationId id);

    List<ClubLocalization> list(int page, int pageSize);

    long count();

    List<ClubLocalization> listAll();
}
