package pl.bbsw.sport.club.club_localization;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ClubLocalizationDaoImpl implements ClubLocalizationDao {
    private NamedParameterJdbcTemplate jdbcTemplate;

    public ClubLocalizationDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public ClubLocalization insert(ClubLocalization clubLocalization) {
        final String sql = "INSERT INTO club_localization" +
                "(ads_city, ads_street, ads_number, ads_postal_code, ads_phone) VALUES" +
                "(:city, :street, :number, :postal_code, :phone)";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("city", clubLocalization.getCity())
                .addValue("street", clubLocalization.getStreet())
                .addValue("number", clubLocalization.getNumber())
                .addValue("postal_code", clubLocalization.getPostalCode())
                .addValue("phone", clubLocalization.getPhone());

        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, params, keyHolder, new String[] {"ads_id"});

        return new ClubLocalizationBuilder(clubLocalization)
                .withId(new ClubLocalizationId(keyHolder.getKey().longValue()))
                .build();
    }

    @Override
    public void update(ClubLocalization clubLocalization) {
        final String sql = "UPDATE club_localization SET " +
                "ads_city = :city, " +
                "ads_street = :street, " +
                "ads_number = :number, " +
                "ads_postal_code = :postal_code, " +
                "ads_phone = :phone " +
                "WHERE ads_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", clubLocalization.getId().id)
                .addValue("city", clubLocalization.getCity())
                .addValue("street", clubLocalization.getStreet())
                .addValue("number", clubLocalization.getNumber())
                .addValue("postal_code", clubLocalization.getPostalCode())
                .addValue("phone", clubLocalization.getPhone());

        final int count = jdbcTemplate.update(sql, params);
        if (count != 1)
            throw new IllegalStateException("Invalid number of columns updated: " + count);
    }

    @Override
    public void delete(ClubLocalizationId clubLocalizationId) {
        final String sql = "DELETE FROM club_localization WHERE ads_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", clubLocalizationId.id);

        final int count = jdbcTemplate.update(sql, params);
        if (count != 1)
            throw new IllegalStateException("Invalid number of rows deleted: " + count);
    }

    @Override
    public Optional<ClubLocalization> get(ClubLocalizationId id) {
        final String sql = "SELECT * FROM club_localization WHERE ads_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id.id);

        return jdbcTemplate.query(sql, params, ClubLocalizationRowMapper.clubLocalization)
                .stream()
                .findFirst();
    }

    @Override
    public List<ClubLocalization> list(int page, int pageSize) {
        final String sql = "SELECT * FROM club_localization ORDER BY ads_id LIMIT :count OFFSET :offset";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("count", pageSize)
                .addValue("offset", (page - 1) * pageSize);

        return jdbcTemplate.query(sql, params, ClubLocalizationRowMapper.clubLocalization);
    }

    @Override
    public long count() {
        final String sql = "SELECT count(1) FROM club_localization";
        return jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong(1)).get(0);
    }

    @Override
    public List<ClubLocalization> listAll() {
        final String sql = "SELECT * FROM club_localization ORDER BY ads_id;";
        return jdbcTemplate.query(sql, ClubLocalizationRowMapper.clubLocalization);
    }
}
