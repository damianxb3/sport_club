package pl.bbsw.sport.club.club_localization;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.bbsw.sport.club.club_localization.dto.ClubLocalizationDto;
import pl.bbsw.sport.club.club_localization.dto.NewClubLocalizationDto;

import java.util.List;
import java.util.Optional;

@Service
public class ClubLocalizationServiceImpl implements ClubLocalizationService{
    private ClubLocalizationDao clubLocalizationDao;

    public ClubLocalizationServiceImpl(ClubLocalizationDao clubLocalizationDao) {
        this.clubLocalizationDao = clubLocalizationDao;
    }

    @Override
    public List<ClubLocalization> list(int page, int pageSize) {
        return clubLocalizationDao.list(page, pageSize);
    }

    @Override
    @Transactional
    public ClubLocalizationId create(NewClubLocalizationDto clubLocalizationData) {
        return clubLocalizationDao.insert(clubLocalizationData.toEntity()).getId();
    }

    @Override
    public long count() {
        return clubLocalizationDao.count();
    }

    @Override
    @Transactional
    public void update(ClubLocalizationDto clubLocalizationData) {
        clubLocalizationDao.update(clubLocalizationData.toEntity());
    }

    @Override
    @Transactional
    public void delete(long id) {
        clubLocalizationDao.delete(new ClubLocalizationId(id));
    }

    @Override
    public Optional<ClubLocalization> get(long id) {
        return clubLocalizationDao.get(new ClubLocalizationId(id));
    }

    @Override
    public List<ClubLocalization> listAll() {
        return clubLocalizationDao.listAll();
    }
}
