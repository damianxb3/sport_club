package pl.bbsw.sport.club.club_localization;

import pl.bbsw.sport.club.club_localization.dto.ClubLocalizationDto;
import pl.bbsw.sport.club.club_localization.dto.NewClubLocalizationDto;

import java.util.List;
import java.util.Optional;

public interface ClubLocalizationService {
    List<ClubLocalization> list(int page, int pageSize);

    ClubLocalizationId create(NewClubLocalizationDto clubLocalization);

    long count();

    void update(ClubLocalizationDto clubLocalization);

    void delete(long id);

    Optional<ClubLocalization> get(long id);

    List<ClubLocalization> listAll();
}
