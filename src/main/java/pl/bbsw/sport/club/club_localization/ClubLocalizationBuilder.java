package pl.bbsw.sport.club.club_localization;

public class ClubLocalizationBuilder {
    private ClubLocalizationId maybeId;
    private String maybeCity;
    private String maybeStreet;
    private String maybeNumber;
    private String maybePostalCode;
    private String maybePhone;

    public ClubLocalizationBuilder(){
    }

    public ClubLocalizationBuilder(ClubLocalization section) {
        this.maybeId = section.getId();
        this.maybeCity = section.getCity();
        this.maybeStreet = section.getStreet();
        this.maybeNumber = section.getNumber();
        this.maybePostalCode = section.getPostalCode();
        this.maybePhone = section.getPhone();
    }

    public ClubLocalizationBuilder withId(ClubLocalizationId id) {
        this.maybeId = id;
        return this;
    }

    public ClubLocalizationBuilder withCity(String city) {
        this.maybeCity = city;
        return this;
    }

    public ClubLocalizationBuilder withStreet(String street) {
        this.maybeStreet = street;
        return this;
    }

    public ClubLocalizationBuilder withNumber(String number) {
        this.maybeNumber = number;
        return this;
    }

    public ClubLocalizationBuilder withPostalCode(String postalCode) {
        this.maybePostalCode = postalCode;
        return this;
    }

    public ClubLocalizationBuilder withPhone(String phone) {
        this.maybePhone = phone;
        return this;
    }

    public ClubLocalization build() {
        assert this.maybeId != null;
        assert maybeCity != null;
        assert maybeStreet != null;
        assert maybeNumber != null;
        assert maybePhone != null;
        return new ClubLocalization(maybeId, maybeCity, maybeStreet, maybeNumber, maybePostalCode, maybePhone);
    }
}
