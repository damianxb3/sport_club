package pl.bbsw.sport.club.club_localization.dto;

import pl.bbsw.sport.club.club_localization.ClubLocalizationId;

public class ClubLocalizationIdDto {
    private long id;

    public ClubLocalizationId toEntity() {
        return new ClubLocalizationId(id);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
