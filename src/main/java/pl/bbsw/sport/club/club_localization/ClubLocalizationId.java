package pl.bbsw.sport.club.club_localization;

public class ClubLocalizationId {
    public final long id;

    public ClubLocalizationId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ClubLocalizationId && ((ClubLocalizationId)o).id == id;
    }

    @Override
    public int hashCode() {
        return ((Long)id).hashCode();
    }

    public static ClubLocalizationId invalid = new ClubLocalizationId(0);
}
