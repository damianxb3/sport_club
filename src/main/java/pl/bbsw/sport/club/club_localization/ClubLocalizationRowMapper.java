package pl.bbsw.sport.club.club_localization;

import org.springframework.jdbc.core.RowMapper;

public class ClubLocalizationRowMapper {
    public static final RowMapper<ClubLocalizationId> clubLocalizationId = (rs, rowId)
            -> new ClubLocalizationId(rs.getLong("ads_id"));

    public static final RowMapper<ClubLocalization> clubLocalization = (rs, rowId) -> new ClubLocalization(
            clubLocalizationId.mapRow(rs, rowId),
            rs.getString("ads_city"),
            rs.getString("ads_street"),
            rs.getString("ads_number"),
            rs.getString("ads_postal_code"),
            rs.getString("ads_phone")
    );
}
