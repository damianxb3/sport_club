package pl.bbsw.sport.club.section;

public class Section {
    private final SectionId id;
    private final SectionType type;

    public Section(SectionId id, SectionType type) {
        this.id = id;
        this.type = type;
    }

    public SectionId getId() {
        return id;
    }

    public SectionType getType() {
        return type;
    }
}
