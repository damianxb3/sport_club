package pl.bbsw.sport.club.section.dto;

import pl.bbsw.sport.club.section.SectionId;

public class SectionIdDto {
    private long id;

    public SectionId toEntity() {
        return new SectionId(id);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
