package pl.bbsw.sport.club.section.dto;

import pl.bbsw.sport.club.section.Section;
import pl.bbsw.sport.club.section.SectionType;

public class SectionDto {
    private SectionIdDto id;
    private SectionType type;

    public Section toEntity() {
        return new Section(id.toEntity(), type);
    }

    public SectionIdDto getId() {
        return id;
    }

    public void setId(SectionIdDto id) {
        this.id = id;
    }

    public SectionType getType() {
        return type;
    }

    public void setType(SectionType type) {
        this.type = type;
    }
}
