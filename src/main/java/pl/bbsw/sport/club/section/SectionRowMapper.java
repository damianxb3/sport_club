package pl.bbsw.sport.club.section;

import org.springframework.jdbc.core.RowMapper;

public class SectionRowMapper {
    public static final RowMapper<SectionId> sectionId = (rs, rowId) -> new SectionId(rs.getLong("sps_id"));

    public static final RowMapper<Section> section = (rs, rowId) -> new Section(
            sectionId.mapRow(rs, rowId),
            SectionType.withId(rs.getString("sps_type"))
    );
}
