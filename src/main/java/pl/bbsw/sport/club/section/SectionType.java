package pl.bbsw.sport.club.section;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;

public enum SectionType {
    Football("football"),
    Archery("archery"),
    Canoeing("canoeing"),
    Volleyball("volleyball"),
    Basketball("basketball"),
    Tennis("tennis");

    public final String id;
    SectionType(String id) {
        this.id = id;
    }

    @JsonCreator
    public static SectionType withId(String id) {
        final Optional<SectionType> result = Arrays.stream(values())
                .filter(v -> v.id.equals(id))
                .findFirst();
        if (!result.isPresent())
            throw new NoSuchElementException();
        return result.get();
    }


    @Override
    @JsonValue
    public String toString() {
        return id;
    }
}
