package pl.bbsw.sport.club.section;

import java.util.List;
import java.util.Optional;

public interface SectionDao {
    Section insert(Section section);

    void update(Section section);

    void delete(SectionId sectionId);

    Optional<Section> get(SectionId id);

    List<Section> list(int page, int pageSize);

    long count();
    
    List<Section> listAll();
}
