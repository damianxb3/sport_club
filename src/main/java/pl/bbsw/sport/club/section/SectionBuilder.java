package pl.bbsw.sport.club.section;

import java.util.Optional;

public class SectionBuilder {
    private SectionId maybeId;
    private SectionType maybeType;

    public SectionBuilder(){
    }

    public SectionBuilder(Section section) {
        this.maybeId = section.getId();
        this.maybeType = section.getType();
    }

    public SectionBuilder withId(SectionId id) {
        this.maybeId = id;
        return this;
    }

    public SectionBuilder withType(SectionType type) {
        this.maybeType = type;
        return this;
    }

    public Section build() {
        assert this.maybeId != null;
        assert this.maybeType != null;
        return new Section(this.maybeId, this.maybeType);
    }
}
