package pl.bbsw.sport.club.section;

import java.util.List;
import java.util.Optional;

public interface SectionService {
    SectionId create(Section section);

    void update(Section section);

    void delete(SectionId sectionId);

    List<Section> list(int page, int pageSize);

    long count();

    Optional<Section> get(SectionId id);

    List<Section> listAll();
}
