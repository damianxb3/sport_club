package pl.bbsw.sport.club.section;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.bbsw.sport.club.section.dto.NewSectionDto;
import pl.bbsw.sport.club.section.dto.SectionDto;
import pl.bbsw.sport.club.util.Pagination;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/section")
@Validated
public class SectionController {
    private SectionService sectionService;

    @Autowired
    public SectionController(SectionService sectionService) {
        this.sectionService = sectionService;
    }

    @PutMapping("/create")
    @ResponseBody
    public SectionId create(@RequestBody NewSectionDto newSectionDto) {
        return sectionService.create(newSectionDto.toEntity());
    }

    @GetMapping("/list/{page}/{pageSize}")
    @ResponseBody
    public List<Section> list(
            @Min(1) @PathVariable int page,
            @Min(1) @Max(Pagination.MAX_PAGE_SIZE) @PathVariable int pageSize
    ) {
        return sectionService.list(page, pageSize);
    }

    @GetMapping("/count")
    @ResponseBody
    public long count() {
        return sectionService.count();
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody SectionDto section) {
        sectionService.update(section.toEntity());
        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/delete/{sectionId}")
    public ResponseEntity<?> delete(@Min(1) @PathVariable long sectionId) {
        sectionService.delete(new SectionId(sectionId));
        return ResponseEntity.ok(null);
    }

    @GetMapping("/get/{sectionId}")
    @ResponseBody
    public Section get(@Min(1) @PathVariable long sectionId) {
        return sectionService.get(new SectionId(sectionId)).orElse(null);
    }

    @GetMapping("/all")
    @ResponseBody
    public List<Section> listAll() {
        return sectionService.listAll();
    }
}
