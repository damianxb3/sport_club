package pl.bbsw.sport.club.section;

public class SectionId {
    public final long id;

    public SectionId(long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return ((Long)id).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof SectionId && ((SectionId)o).id == id;
    }

    @Override
    public String toString() {
        return ((Long)id).toString();
    }

    public static SectionId invalid = new SectionId(0);
}
