package pl.bbsw.sport.club.section;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SectionServiceImpl implements SectionService {
    private SectionDao sectionDao;

    @Autowired
    public SectionServiceImpl(SectionDao sectionDao) {
        this.sectionDao = sectionDao;
    }

    @Override
    public SectionId create(Section section) {
        return sectionDao.insert(section).getId();
    }

    @Override
    public List<Section> list(int page, int pageSize) {
        return sectionDao.list(page, pageSize);
    }

    @Override
    public long count() {
        return sectionDao.count();
    }

    @Override
    public void update(Section section) {
        sectionDao.update(section);
    }

    @Override
    public void delete(SectionId sectionId) {
        sectionDao.delete(sectionId);
    }

    @Override
    public Optional<Section> get(SectionId id) {
        return sectionDao.get(id);
    }

    @Override
    public List<Section> listAll() {
        return sectionDao.listAll();
    }
}
