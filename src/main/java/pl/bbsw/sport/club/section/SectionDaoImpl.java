package pl.bbsw.sport.club.section;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class SectionDaoImpl implements SectionDao {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public SectionDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Section insert(Section section) {
        final String sql = "INSERT INTO sport_section(sps_type) VALUES (:type)";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("type", section.getType().id);

        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, params, keyHolder, new String[] {"sps_id"});

        return new SectionBuilder(section)
                .withId(new SectionId(keyHolder.getKey().longValue()))
                .build();
    }

    @Override
    public void update(Section section) {
        final String sql = "UPDATE sport_section SET sps_type = :type WHERE sps_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", section.getId().id)
                .addValue("type", section.getType().id);

        final int count = jdbcTemplate.update(sql, params);
        if (count != 1)
            throw new IllegalStateException("Invalid number of columns updated: " + count);
    }

    @Override
    public void delete(SectionId sectionId) {
        final String sql = "DELETE FROM sport_section WHERE sps_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", sectionId.id);

        final int count = jdbcTemplate.update(sql, params);
        if (count != 1)
            throw new IllegalStateException("Invalid number of rows deleted: " + count);
    }

    @Override
    public Optional<Section> get(SectionId id) {
        final String sql = "SELECT * FROM sport_section WHERE sps_id = :id";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id.id);

        return jdbcTemplate.query(sql, params, SectionRowMapper.section)
                .stream()
                .findFirst();
    }

    @Override
    public List<Section> list(int page, int pageSize) {
        final String sql = "SELECT * FROM sport_section ORDER BY sps_id LIMIT :count OFFSET :offset";
        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("count", pageSize)
                .addValue("offset", (page - 1) * pageSize);

        return jdbcTemplate.query(sql, params, SectionRowMapper.section);
    }

    @Override
    public long count() {
        final String sql = "SELECT count(1) FROM sport_section";
        return jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong(1)).get(0);
    }
    
    @Override
    public List<Section> listAll() {
        final String sql = "SELECT * FROM sport_section ORDER BY sps_id";
        return jdbcTemplate.query(sql, SectionRowMapper.section);
    }
}
