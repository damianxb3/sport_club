package pl.bbsw.sport.club.section.dto;

import pl.bbsw.sport.club.section.Section;
import pl.bbsw.sport.club.section.SectionId;
import pl.bbsw.sport.club.section.SectionType;

public class NewSectionDto {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Section toEntity() {
        return new Section(SectionId.invalid, SectionType.withId(type));
    }
}
