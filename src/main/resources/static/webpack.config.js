module.exports = {
    entry: "./app/index.tsx",
    output: {
        filename: "./dist/bundle.js"
    },

    devtool: "source-map",
    devServer: {
        historyApiFallback: true
    },

    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            {test: /\.tsx?$/, loader: "ts-loader"}
        ],

        preLoaders: [
            {test: /\.js$/, loader: "source-map-loader"}
        ]
    },

    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
        "redux": "Redux",
        "react-redux": "ReactRedux",
        "ReactRouter": "ReactRouter",
        "redux-thunk": "ReduxThunk",
        "immutable": "Immutable",
        "jquery": "jQuery",
        "bootstrap": "Bootstrap"
    }
};