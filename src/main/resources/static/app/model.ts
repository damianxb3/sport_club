export interface AddressId {
    id: number;
}

export interface Address {
    id: AddressId;
    city: string;
    street: string;
    number: string;
    postalCode: string;
    phone: string;
}

export type EventType = "Competition" | "Camp" | "Gathering";
export class EventTypes {
    public static readonly Competition: EventType = "Competition";
    public static readonly Camp: EventType = "Camp";
    public static readonly Gathering: EventType = "Gathering";

    public static readonly values: EventType[] = [
        EventTypes.Competition,
        EventTypes.Camp,
        EventTypes.Gathering
    ];
}

export interface EventId {
    id: number;
}

export interface Event {
    id: EventId;
    address: Address;
    type: EventType;
    name: string;
    description: string;
    startDate: Date;
    endDate: Date;
}

export type SportSectionType = "football" | "archery" | "canoeing" | "volleyball" | "basketball" | "tennis";
export class SportSectionTypes {
    public static readonly Football: SportSectionType = "football";
    public static readonly Archery: SportSectionType = "archery";
    public static readonly Canoeing: SportSectionType = "canoeing";
    public static readonly Volleyball: SportSectionType = "volleyball";
    public static readonly Basketball: SportSectionType = "basketball";
    public static readonly Tennis: SportSectionType = "tennis";

    public static readonly values: SportSectionType[] = [
        SportSectionTypes.Football,
        SportSectionTypes.Archery,
        SportSectionTypes.Canoeing,
        SportSectionTypes.Volleyball,
        SportSectionTypes.Basketball,
        SportSectionTypes.Tennis
    ];
}

export interface SportSectionId {
    id: number;
}

export interface SportSection {
    id: SportSectionId;
    type: SportSectionType;
}

export interface TrainingGroupId {
    id: number;
}

export interface TrainingGroup {
    id: TrainingGroupId;
    section: SportSection;
    monthlyFee: number;
}

export interface TrainingDate {

}

export interface GroupProperty {

}

export interface StaffMember {

}

export interface GroupMember {

}

export interface Payment {

}

export interface Trophy {

}

export interface EventAssignment {
    id: number;
    event: Event;

}