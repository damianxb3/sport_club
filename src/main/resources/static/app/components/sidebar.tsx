import * as React from "react";
import { Link } from 'react-router';

const SidebarItem = (props: {name: string, target: string, is: boolean}) =>
    <li className={props.is ? "active" : ""}>
        <Link to={props.target}>{props.name} {props.is ? <span className="sr-only">(current)</span> : <span/>}</Link>
    </li>;

export interface SidebarProps {
    urlHashPart: string;
}

export class Sidebar extends React.Component<SidebarProps, undefined> {
    public render(): JSX.Element {
        return <div className="col-sm-3 col-md-2 sidebar">
            <ul className="nav nav-sidebar">
                <SidebarItem name="Podsumowanie" target="summary" is={this.isSummarySection()}/>
            </ul>
            <ul className="nav nav-sidebar">
                <SidebarItem name="Sekcje" target="sections" is={this.isSectionsScreen()}/>
                <SidebarItem name="Placówki" target="clubs" is={this.isClubsScreen()}/>
                <SidebarItem name="Grupy" target="groups" is={this.isGroupsScreen()}/>
                <SidebarItem name="Wydarzenia" target="events" is={this.isEventsScreen()}/>
            </ul>
            <ul className="nav nav-sidebar">
                <SidebarItem name="Członkowie" target="members" is={this.isMembersScreen()}/>
            </ul>
            <ul className="nav nav-sidebar">
                <SidebarItem name="Pracownicy" target="staff" is={this.isStaffScreen()}/>
            </ul>
        </div>
    }

    private isSummarySection(): boolean {
        return this.props.urlHashPart === "" || this.startWithHash('summary');
    }

    private isSectionsScreen(): boolean {
        return this.startWithHash('sections');
    }

    private isClubsScreen(): boolean {
        return this.startWithHash('clubs');
    }

    private isGroupsScreen(): boolean {
        return this.startWithHash('groups');
    }

    private isEventsScreen() {
        return this.startWithHash('events');
    }

    private isMembersScreen(): boolean {
        return this.startWithHash('members');
    }

    private isStaffScreen(): boolean {
        return this.startWithHash('staff');
    }

    private startWithHash(hash: string): boolean {
        return this.props.urlHashPart.substr(2, hash.length) === hash;
    }
}