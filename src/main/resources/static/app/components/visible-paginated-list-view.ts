import {connect} from 'react-redux'
import {makePaginatedListView} from "./paginated-list-view";
import {StoreState} from "../reducers/root.reducer";
import {Dispatch} from "redux";
import {ListViewItem} from "./list-view-item";

export interface VisiblePaginatedListViewProps<T> {
    visibleItems: T[];
    page: number;
    totalPages: number;
}

export function makeVisiblePaginatedListView<T>(
    propMapper: (state: StoreState) => VisiblePaginatedListViewProps<T>,
    itemPresenter: new() => ListViewItem<T>,
    itemKeyMapper: (item: T) => string,
    itemClick: (dispatch: Dispatch<StoreState>, item: T) => void,
    nextPage: (dispatch: Dispatch<StoreState>) => void,
    prevPage: (dispatch: Dispatch<StoreState>) => void
): React.ComponentClass<{}> {
    const mapStateToProps = (state: StoreState) => {
        const props = propMapper(state);
        return {
            items: props.visibleItems,
            itemPresenter: itemPresenter,
            itemKeyMapper: itemKeyMapper,
            page: props.page,
            totalPages: props.totalPages
        };
    };

    const mapDispatchToProps = (dispatch: Dispatch<StoreState>) => {
        return {
            onClick: (item: T) => itemClick(dispatch, item),
            nextPage: () => nextPage(dispatch),
            prevPage: () => prevPage(dispatch)
        };
    };

    return connect(
        mapStateToProps,
        mapDispatchToProps
    )(makePaginatedListView<T>());
}
