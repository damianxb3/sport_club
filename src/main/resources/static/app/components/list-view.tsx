import * as React from "react";
import {ListViewItem} from "./list-view-item";

export interface ListViewProps<T> {
    items: T[];
    itemPresenter: new() => ListViewItem<T>;
    keyMapper: (item: T) => string;

    onClick: (item: T) => void;
}

export function makeListView<T>(): React.StatelessComponent<ListViewProps<T>> {
    return ({items, itemPresenter, keyMapper, onClick, children}: ListViewProps<T> & {children?: any}) => {
        const ItemPresenter = itemPresenter;
        return <table className="table table-striped table-hover">
            <thead>
            {children}
            </thead>

            <tbody>
            {items.map(item => <ItemPresenter item={item} onClick={onClick} key={keyMapper(item)}/>)}
            </tbody>
        </table>;
    };
}