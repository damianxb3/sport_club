import * as React from "react";

export interface SelectTextProps {
    value: string;

    handleChange: (newValue: string) => void;
}

export const makeSelectText = (text: string) => {
    const id: string = Math.random().toString();

    return ({value, handleChange}: SelectTextProps) => {
        return <div className="form-group">
            <label htmlFor={id}>{text}</label>
            <input id={id}
                   className="form-control"
                   type="text"
                   value={value ? value : ""}
                   onChange={(e: any) => handleChange(e.target.value)}
            />
        </div>
    };
};
