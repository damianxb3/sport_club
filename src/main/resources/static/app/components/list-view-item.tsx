import * as React from "react";

export interface ListViewItemProps<T> {
    item: T;
    onClick: (item: T) => void;
}

export abstract class ListViewItem<T> extends React.Component<ListViewItemProps<T>, undefined> {
    private onclick = () => this.props.onClick(this.props.item);

    public render(): JSX.Element {
        return <tr onClick={this.onclick} className={this.getRowClass()}>
            {this.renderCells()}
        </tr>;
    }

    protected abstract renderCells(): JSX.Element[];

    protected getRowClass(): string {
        return "";
    }
}