import * as React from "react";
import {SportSectionTypes, SportSectionType} from "../../model";
import {mapSectionTypeText} from "./section-type-text";

export interface SelectSportSectionTypeProps {
    type: SportSectionType;

    handleTypeChange: (type: SportSectionType) => void;
}

export const SelectSportSectionType = ({type, handleTypeChange}: SelectSportSectionTypeProps) => {
    const id: string = Math.random().toString();

    return <div className="form-group">
            <label htmlFor={id}>Wybierz typ sekcji</label>
            <select className="form-control"
                    id={id}
                    value={type || ""}
                    onChange={(e: any) => handleTypeChange(e.target.value !== "" ? e.target.value : undefined)}>
                <option value="">Wybierz typ sekcji</option>

                {SportSectionTypes.values.map(section => {
                    return <option value={section} key={section}>
                        {mapSectionTypeText(section)}
                    </option>
                })}
            </select>
        </div>
};
