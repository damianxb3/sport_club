import * as React from "react";
import {ListViewItem} from "../list-view-item";
import {SportSection} from "../../model";
import {SectionTypeText} from "./section-type-text";

export class SectionListViewItem extends ListViewItem<SportSection> {
    protected renderCells(): JSX.Element[] {
        return [
            <td key="1">{this.props.item.id.id}</td>,
            <td key="2"><SectionTypeText sectionType={this.props.item.type}/></td>
        ];
    }


    protected getRowClass(): string {
        return "clickable";
    }
}