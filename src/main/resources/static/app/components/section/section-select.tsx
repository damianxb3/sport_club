import * as React from "react";
import {SportSection} from "../../model";
import {mapSectionTypeText} from "./section-type-text";

export interface SectionSelectProps {
    allSections: SportSection[];
    selected?: number;

    onSelect: (section: SportSection) => void;
}

export const makeSectionSelect = () => {
    const id: string = Math.random().toString();

    return ({allSections, selected, onSelect}: SectionSelectProps) => {
        return <div className="form-group">
            <label htmlFor={id}>Wybierz sekcję</label>
            <select className="form-control"
                    id={id}
                    value={selected || ""}
                    onChange={(e: any) => onSelect(e.target.value !== "" ? e.target.value : undefined)}>
                <option value="">Wybierz sekcję</option>

                {allSections.map(section => {
                    return <option value={section.id.id} key={section.id.id}>
                        {section.id.id}: {mapSectionTypeText(section.type)}
                    </option>
                })}
            </select>
        </div>
    };
}