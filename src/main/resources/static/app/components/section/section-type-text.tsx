import * as React from "react";
import {SportSectionTypes, SportSectionType} from "../../model";

export function mapSectionTypeText(type: SportSectionType): string {
    const mappings = {
        [SportSectionTypes.Football]: "Piłka nożna",
        [SportSectionTypes.Archery]: "Łucznicza",
        [SportSectionTypes.Canoeing]: "Kajakarska",
        [SportSectionTypes.Volleyball]: "Siatkowa",
        [SportSectionTypes.Basketball]: "Koszykowa",
        [SportSectionTypes.Tennis]: "Tenis ziemny"
    };
    return mappings[type];
}

export interface SectionTypeTextProps {
    sectionType: SportSectionType;
    upperFirstLetter?: boolean;
}

export class SectionTypeText extends React.Component<SectionTypeTextProps, undefined> {
    public render(): JSX.Element {
        const text = mapSectionTypeText(this.props.sectionType);
        const upperFirst = this.props.upperFirstLetter || this.props.upperFirstLetter === undefined;
        return <span>{upperFirst ? text : text.toLowerCase()}</span>
    }
}