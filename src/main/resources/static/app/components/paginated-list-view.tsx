import * as React from "react";

import {makeListView, ListViewProps} from "./list-view";
import {ListViewItem} from "./list-view-item";

export interface PaginatedListViewProps<T> {
    items: T[];
    itemPresenter: new() => ListViewItem<T>;
    itemKeyMapper: (item: T) => string;
    page: number;
    totalPages: number;

    onClick: (item: T) => void;
    nextPage: () => void;
    prevPage: () => void;
}

export function makePaginatedListView<T>(): React.StatelessComponent<PaginatedListViewProps<T>> {
    return (
        {
            items,
            itemPresenter,
            itemKeyMapper,
            page,
            totalPages,
            onClick,
            nextPage,
            prevPage,
            children
        }: PaginatedListViewProps<T> & {children: any}) => {
        const ListView: React.StatelessComponent<ListViewProps<T>> = makeListView<T>();

        return <div>
            <div className="row">
                <ListView items={items}
                          itemPresenter={itemPresenter}
                          keyMapper={itemKeyMapper}
                          onClick={onClick}>
                    {children}
                </ListView>
            </div>

            <div className="row">
                <button type="button"
                        className="col-sm-1 col-sm-offset-4"
                        onClick={prevPage}
                        disabled={page <= 1}>
                    {'<'}
                </button>

                <div className="col-sm-2 text-center">
                    {page} / {totalPages}
                </div>

                <button type="button"
                        className="col-sm-1"
                        onClick={nextPage}
                        disabled={page >= totalPages}>
                    {'>'}
                </button>
            </div>
        </div>;
    }
}