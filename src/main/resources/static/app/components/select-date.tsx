import * as React from "react";
import * as DatePicker from 'react-datepicker';
import * as moment from 'moment';

export interface SelectDateProps {
    value: Date;

    handleChange: (newValue: Date) => void;
}

export const makeSelectDate = (label: string) => {
    const id: string = Math.random().toString();

    return ({value, handleChange}: SelectDateProps) => {
        return <div className="form-group">
            <label htmlFor={id}>{label}</label>
            <DatePicker
                id={id}
                dateFormat="YYYY/MM/DD"
                selected={moment(value)}
                onChange={handleChange}
                className="form-control"
            />
        </div>
    };
};
