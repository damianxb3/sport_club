import * as React from "react";
import {ListViewItem} from "../list-view-item";
import {Address} from "../../model";

export class AddressListViewItem extends ListViewItem<Address> {
    protected renderCells(): JSX.Element[] {
        return [
            <td key="1">{this.props.item.id.id}</td>,
            <td key="2">{this.props.item.city}</td>,
            <td key="3">{this.props.item.street}</td>,
            <td key="4">{this.props.item.number}</td>,
            <td key="5">{this.props.item.postalCode}</td>,
            <td key="6">{this.props.item.phone}</td>
        ];
    }


    protected getRowClass(): string {
        return "clickable";
    }
}