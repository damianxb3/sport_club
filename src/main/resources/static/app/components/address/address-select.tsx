import * as React from "react";
import {Address} from "../../model";

export interface AddressSelectProps {
    allAddresses: Address[];
    selected?: number;

    onSelect: (address: Address) => void;
}

export const makeAddressSelect = () => {
    const id: string = Math.random().toString();

    return ({allAddresses, selected, onSelect}: AddressSelectProps) => {
        return <div className="form-group">
            <label htmlFor={id}>Wybierz lokalizację</label>
            <select className="form-control"
                    id={id}
                    value={selected || ""}
                    onChange={(e: any) => onSelect(e.target.value !== "" ? e.target.value : undefined)}>
                <option value="">Wybierz lokalizację</option>

                {allAddresses.map(address => {
                    return <option value={address.id.id} key={address.id.id}>
                        {address.id.id}: {address.city}: {address.street} {address.number}
                    </option>
                })}
            </select>
        </div>
    };
}