import * as React from "react";
import {ListViewItem} from "../list-view-item";
import {Event} from "../../model";
import {EventTypeText} from "./event-type-text";

export class EventListViewItem extends ListViewItem<Event> {
    protected renderCells(): JSX.Element[] {
        return [
            <td key="1">{this.props.item.id.id}</td>,
            <td key="2">{this.props.item.address.city}, {this.props.item.address.street} {this.props.item.address.number}</td>,
            <td key="3"><EventTypeText eventType={this.props.item.type}/></td>,
            <td key="4">{this.props.item.name}</td>,
            <td key="5">{this.props.item.description}</td>,
            <td key="6">{this.props.item.startDate}</td>,
            <td key="7">{this.props.item.endDate}</td>
        ];
    }


    protected getRowClass(): string {
        return "clickable";
    }
}