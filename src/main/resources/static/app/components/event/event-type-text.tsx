import * as React from "react";
import {EventType, EventTypes} from "../../model";

export function mapEventTypeText(type: EventType): string {
    const mappings = {
        [EventTypes.Competition]: "Zawody",
        [EventTypes.Camp]: "Obóz",
        [EventTypes.Gathering]: "Zgromadzenie"
    };
    return mappings[type];
}

export interface EventTypeTextProps {
    eventType: EventType;
    upperFirstLetter?: boolean;
}

export class EventTypeText extends React.Component<EventTypeTextProps, undefined> {
    public render(): JSX.Element {
        const text = mapEventTypeText(this.props.eventType);
        const upperFirst = this.props.upperFirstLetter || this.props.upperFirstLetter === undefined;
        return <span>{upperFirst ? text : text.toLowerCase()}</span>
    }
}