import * as React from "react";
import {EventTypes, EventType} from "../../model";
import {mapEventTypeText} from "./event-type-text";

export interface SelectEventTypeProps {
    type: EventType;

    handleTypeChange: (type: EventType) => void;
}

export const SelectEventType = ({type, handleTypeChange}: SelectEventTypeProps) => {
    const id: string = Math.random().toString();

    return <div className="form-group">
            <label htmlFor={id}>Wybierz typ wydarzenia</label>
            <select className="form-control"
                    id={id}
                    value={type || ""}
                    onChange={(e: any) => handleTypeChange(e.target.value !== "" ? e.target.value : undefined)}>
                <option value="">Wybierz typ wydarzenia</option>

                {EventTypes.values.map(event => {
                    return <option value={event} key={event}>
                        {mapEventTypeText(event)}
                    </option>
                })}
            </select>
        </div>
};
