import * as React from "react";

import {ListViewItem} from "../list-view-item";
import {TrainingGroup} from "../../model";
import {mapSectionTypeText} from "../section/section-type-text";

export class TrainingGroupListViewItem extends ListViewItem<TrainingGroup> {
    protected renderCells(): JSX.Element[] {
        return [
            <td key="1">{this.props.item.id.id}</td>,
            <td key="2">{mapSectionTypeText(this.props.item.section.type)}</td>,
            <td key="3">{this.props.item.monthlyFee} PLN</td>
        ];
    }

    protected getRowClass(): string {
        return "clickable";
    }
}