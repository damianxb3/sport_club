import * as React from "react";
import {connect, Dispatch} from "react-redux";
import {StoreState} from "../../reducers/root.reducer";
import {CrudActions} from "../../actions/crud.action";
import {WithId} from "../../util/with-id";
import {CrudEditFieldProps} from "./crud-edit";

export interface CrudAddProps<NewItemType> {
    state: StoreState;

    newItem: NewItemType;
    fields: React.StatelessComponent<CrudEditFieldProps<NewItemType>>[],

    validate: (newItem: NewItemType) => boolean;

    handleAdd: (newItem: NewItemType) => void;
    handleFieldChange: (field: string, value: any) => void;
}

function makeCrudAdd<NewItemType>() {
    return ({state, newItem, fields, validate, handleAdd, handleFieldChange}: CrudAddProps<NewItemType>) => {
        return <form>
            {fields.map((Field, i) => <Field
                key={i}
                state={state}
                item={newItem}
                handleFieldChange={handleFieldChange}
            />)}

            <button type="submit"
                    className="btn btn-primary"
                    onClick={(e) => {e.preventDefault(); handleAdd(newItem)}}
                    disabled={!validate(newItem)}>
                Dodaj
            </button>
        </form>;
    };
}

export function makeVisibleCrudAdd<ItemType, NewItemType, EditItemType extends WithId>(
    domainName: string,
    actions: CrudActions<ItemType, NewItemType, EditItemType>,
    fields: React.StatelessComponent<CrudEditFieldProps<EditItemType>>[],
    validate: (newItem: NewItemType) => boolean
) {
    return connect(
        (state: StoreState) => {
            return {
                state: state,
                newItem: (state as any)[domainName].newItem,
                fields: fields
            };
        },
        (dispatch: Dispatch<StoreState>) => {
            return {
                validate: validate,
                handleAdd: (newItem: NewItemType) => dispatch(actions.add(newItem)),
                handleFieldChange: (field: string, value: any) => dispatch(actions.setAddField(field, value))
            };
        }
    )(makeCrudAdd());
}
