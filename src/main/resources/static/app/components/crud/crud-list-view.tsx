import * as React from "react";
import {makeVisiblePaginatedListView} from "../visible-paginated-list-view";
import {StoreState} from "../../reducers/root.reducer";
import {Dispatch} from "react-redux";
import {ListViewItem} from "../list-view-item";
import {CrudState} from "../../reducers/crud.reducer";
import {CrudActions} from "../../actions/crud.action";
import {WithId} from "../../util/with-id";

export function makeCrudListView<ItemType extends WithId, NewItemType, EditItemType extends WithId>(
    domainName: string,
    actions: CrudActions<ItemType, NewItemType, EditItemType>,
    columns: string[],
    itemPresenter: new() => ListViewItem<ItemType>
) {
    return () => {
        const View = makeVisiblePaginatedListView<ItemType>(
            (state: StoreState) => {
                const domainState: CrudState<ItemType, any, any> = (state as any)[domainName];
                return {
                    visibleItems: domainState.visibleItems,
                    page: domainState.listPage,
                    totalPages: Math.max(1, Math.ceil(domainState.totalCount / domainState.listPageSize))
                };
            },
            itemPresenter,
            (item: ItemType) => item.id.id.toString(),
            (dispatch: Dispatch<StoreState>, item: ItemType) => dispatch(actions.listItemClick(item)),
            (dispatch: Dispatch<StoreState>) => dispatch(actions.listNext()),
            (dispatch: Dispatch<StoreState>) => dispatch(actions.listPrevious())
        );

        return <View>
            <tr>
                {columns.map(name => <th key={name}>{name}</th>)}
            </tr>
        </View>
    }
}
