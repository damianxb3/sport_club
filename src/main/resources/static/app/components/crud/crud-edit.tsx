import * as React from "react";
import {connect, Dispatch} from "react-redux";
import {StoreState} from "../../reducers/root.reducer";
import {CrudActions} from "../../actions/crud.action";
import {WithId} from "../../util/with-id";

export interface CrudEditProps<EditItemType> {
    state: StoreState;

    editItem: EditItemType;
    fields: React.StatelessComponent<CrudEditFieldProps<EditItemType>>[],

    validate: (editItem: EditItemType) => boolean;

    handleEdit: (editItem: EditItemType) => void;
    handleDelete: (editItem: EditItemType) => void;
    handleCancel: (editItem: EditItemType) => void;
    handleFieldChange: (field: string, value: any) => void;
}

export interface CrudEditFieldProps<EditItemType> {
    state: StoreState;
    item: EditItemType;
    handleFieldChange: (field: string, value: any) => void;
}

function makeCrudEdit<EditItemType>() {
    return ({
        state,
        editItem,
        fields,
        validate,
        handleEdit,
        handleDelete,
        handleCancel,
        handleFieldChange
    }: CrudEditProps<EditItemType>) => {
        return <form>
            {fields.map((Field, i) => <Field key={i} state={state} item={editItem} handleFieldChange={handleFieldChange}/>)}

            <button type="submit"
                    className="btn btn-primary"
                    onClick={(e) => {e.preventDefault(); handleEdit(editItem)}}
                    disabled={!validate(editItem)}>
                Edytuj
            </button>

            <button type="button"
                    className="btn btn-danger"
                    onClick={() => handleDelete(editItem)}>
                Usuń
            </button>

            <button type="button"
                    className="btn btn-primary"
                    onClick={() => handleCancel(editItem)}>
                Anuluj
            </button>
        </form>;
    };
}

export function makeVisibleCrudEdit<ItemType, NewItemType, EditItemType extends WithId>(
    domainName: string,
    actions: CrudActions<ItemType, NewItemType, EditItemType>,
    fields: React.StatelessComponent<CrudEditFieldProps<EditItemType>>[],
    validate: (editItem: EditItemType) => boolean
) {
    return connect(
        (state: StoreState) => {
            return {
                state: state,
                editItem: (state as any)[domainName].editedItem as EditItemType,
                fields: fields
            };
        },
        (dispatch: Dispatch<StoreState>) => {
            return {
                validate: validate,
                handleEdit: (editItem: EditItemType) => dispatch(actions.edit(editItem)),
                handleDelete: (editItem: EditItemType) => dispatch(actions.remove(editItem.id.id)),
                handleCancel: (editItem: EditItemType) => dispatch(actions.cancelEdit()),
                handleFieldChange: (field: string, value: any) => dispatch(actions.setEditField(field, value))
            };
        }
    )(makeCrudEdit());
}
