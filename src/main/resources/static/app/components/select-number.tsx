import * as React from "react";

export interface SelectNumberProps {
    value: number;

    handleChange: (newValue: number) => void;
}

export const makeSelectNumber = (text: string, min: number = undefined, max: number = undefined) => {
    const id: string = Math.random().toString();

    return ({value, handleChange}: SelectNumberProps) => {
        return <div className="form-group">
            <label htmlFor={id}>{text}</label>
            <input id={id}
                   className="form-control"
                   type="number"
                   step="any"
                   value={value ? value : ""}
                   min={min}
                   max={max}
                   onChange={(e: any) => handleChange(e.target.value)}
            />
        </div>
    };
}
