import "redux-thunk";
import {ThunkAction} from "redux";
import {Action} from "redux";
import {Dispatch} from "react-redux";
import {StoreState} from "../reducers/root.reducer";
import {CrudState} from "../reducers/crud.reducer";
import {CrudApi} from "../api/crud.api";
import {WithId} from "../util/with-id";

export const CrudActionTypes = {
    CRUD_SET_LIST_ITEMS: 'CRUD_SET_LIST_ITEMS',
    CRUD_LIST_ITEM_CLICK: 'CRUD_LIST_ITEM_CLICK',
    CRUD_ADD_FINISH: 'CRUD_ADD_FINISH',
    CRUD_ADD_FIELD_SET: 'CRUD_ADD_FIELD_SET',
    CRUD_EDIT_FINISH: 'CRUD_EDIT_FINISH',
    CRUD_EDIT_FIELD_SET: 'CRUD_EDIT_FIELD_SET',
    CRUD_DELETE: 'CRUD_DELETE',
    CRUD_CANCEL_EDIT: 'CRUD_CANCEL_EDIT'
};

export interface CountAndListResult<ItemType> {
    items: ItemType[];
    totalCount: number;
    page: number;
    pageSize: number;
}

export class CrudActions<ItemType, NewItemType, EditItemType extends WithId> {
    constructor(
        private domainName: string,
        private api: CrudApi<ItemType, NewItemType, EditItemType>
    ) {}

    public enterScreen() {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            const state = this.getState(getState());
            return this.countAndList(state.listPage, state.listPageSize)
                .then(result => this.addCrudDomain(this.addType(result, CrudActionTypes.CRUD_SET_LIST_ITEMS)))
                .then(dispatch);
        };
    }

    public listPrevious() {
        return this.list(-1);
    }

    public listNext() {
        return this.list(+1);
    }

    private list(offset: number) {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            const state = this.getState(getState());
            return this.countAndList(state.listPage + offset, state.listPageSize)
                .then(result => this.addCrudDomain(this.addType(result, CrudActionTypes.CRUD_SET_LIST_ITEMS)))
                .then(dispatch);
        };
    }

    public setAddField(field: string, value: any) {
        return this.addCrudDomain({
            type: CrudActionTypes.CRUD_ADD_FIELD_SET,
            field: field,
            value: value
        });
    }

    public add(newItem: NewItemType) {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            return this.api.create(newItem)
                .then(() => dispatch(this.addCrudDomain({type: CrudActionTypes.CRUD_ADD_FINISH})))
                .then(() => this.reloadItems(dispatch, getState));
        };
    }

    public setEditField(field: string, value: any) {
        return this.addCrudDomain({
            type: CrudActionTypes.CRUD_EDIT_FIELD_SET,
            field: field,
            value: value
        });
    }

    public edit(editItem: EditItemType) {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            return this.api.update(editItem)
                .then(() => dispatch(this.addCrudDomain({type: CrudActionTypes.CRUD_EDIT_FINISH})))
                .then(() => this.reloadItems(dispatch, getState));
        };
    }

    public remove(id: number) {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            return this.api.remove(id)
                .then(() => dispatch(this.addCrudDomain({type: CrudActionTypes.CRUD_DELETE})))
                .then(() => this.reloadItems(dispatch, getState));
        };
    }

    public cancelEdit() {
        return this.addCrudDomain({
            type: CrudActionTypes.CRUD_CANCEL_EDIT
        });
    }

    private reloadItems(dispatch: Dispatch<StoreState>, getState: () => StoreState) {
        const state = this.getState(getState());
        return this.countAndList(state.listPage, state.listPageSize)
            .then(result => this.addCrudDomain(this.addType(result, CrudActionTypes.CRUD_SET_LIST_ITEMS)))
            .then(dispatch);
    }

    public listItemClick(item: ItemType) {
        return this.addCrudDomain({
            type: CrudActionTypes.CRUD_LIST_ITEM_CLICK,
            item: item
        });
    }

    private addType<T>(result: T, type: string): T & Action {
        return Object.assign({}, result, {type: type});
    }

    private addCrudDomain<T>(result: T): T & {crudDomain: string} {
        return Object.assign({}, result, {crudDomain: this.domainName});
    }

    private countAndList(page: number, pageSize: number): Promise<CountAndListResult<ItemType>> {
        return this.api.count().then(count => {
            return this.api.list(page, pageSize).then(items => {
                return <CountAndListResult<ItemType>>{
                    items: items,
                    totalCount: count,
                    page: page,
                    pageSize: pageSize
                };
            });
        })
    }

    private getState(state: StoreState): CrudState<ItemType, NewItemType, EditItemType> {
        return (state as any)[this.domainName] as CrudState<ItemType, NewItemType, EditItemType>;
    }
}
