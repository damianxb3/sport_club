import "redux-thunk";
import {SportSection} from "../model";
import {SectionApi} from "../api/section.api";
import {NewSectionDto} from "../dto/section.dto";
import {CrudActions} from "./crud.action";

export const SectionActions = new CrudActions<SportSection, NewSectionDto, SportSection>("section", SectionApi);
