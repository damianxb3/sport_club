import "redux-thunk";
import {Address} from "../model";
import {AddressApi} from "../api/address.api";
import {NewAddressDto, AddressUpdateDto} from "../dto/address.dto";
import {CrudActions} from "./crud.action";

export const AddressActions = new CrudActions<Address, NewAddressDto, AddressUpdateDto>("address", AddressApi);
