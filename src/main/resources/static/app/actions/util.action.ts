import "redux-thunk";
import {Dispatch} from "react-redux";
import {StoreState} from "../reducers/root.reducer";
import {SectionApi} from "../api/section.api";
import {AddressApi} from "../api/address.api";

export const UtilActionTypes = {
    UTIL_LOAD_ALL_SECTIONS: 'UTIL_LOAD_ALL_SECTIONS',
    UTIL_LOAD_ALL_ADDRESSES: 'UTIL_LOAD_ALL_ADDRESSES'
};

export class UtilActions {
    public static loadAllSections() {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            SectionApi.listAll().then(sections => dispatch({
                type: UtilActionTypes.UTIL_LOAD_ALL_SECTIONS,
                allSections: sections
            }));
        };
    }

    public static loadAllAddresses() {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            AddressApi.listAll().then(addresses => dispatch({
                type: UtilActionTypes.UTIL_LOAD_ALL_ADDRESSES,
                allAddresses: addresses
            }));
        }
    }
}