import "redux-thunk";
import {Dispatch} from "react-redux";
import {StoreState} from "../reducers/root.reducer";
import {CrudApi} from "../api/crud.api";
import {TrainingGroup} from "../model";
import {TrainingGroupApi} from "../api/training-group.api";
import {NewTrainingGroupDto, TrainingGroupUpdateDto} from "../dto/training-group.dto";
import {CrudActions} from "./crud.action";
import {UtilActions} from "./util.action";

class TrainingGroupActionsClass extends CrudActions<TrainingGroup, NewTrainingGroupDto, TrainingGroupUpdateDto> {
    constructor(domainName: string, api: CrudApi<TrainingGroup, NewTrainingGroupDto, TrainingGroupUpdateDto>) {
        super(domainName, api);
    }

    public enterScreen() {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            UtilActions.loadAllSections()(dispatch, getState);
            return super.enterScreen()(dispatch, getState);
        };
    }
}


export const TrainingGroupActions = new TrainingGroupActionsClass("trainingGroup", TrainingGroupApi);
