import "redux-thunk";
import {Dispatch} from "react-redux";
import {StoreState} from "../reducers/root.reducer";
import {CrudApi} from "../api/crud.api";
import {Event} from "../model";
import {EventApi} from "../api/event.api";
import {NewEventDto, EventUpdateDto} from "../dto/event.dto";
import {CrudActions} from "./crud.action";
import {UtilActions} from "./util.action";

class EventActionsClass extends CrudActions<Event, NewEventDto, EventUpdateDto> {
    constructor(domainName: string, api: CrudApi<Event, NewEventDto, EventUpdateDto>) {
        super(domainName, api);
    }

    public enterScreen() {
        return (dispatch: Dispatch<StoreState>, getState: () => StoreState) => {
            UtilActions.loadAllAddresses()(dispatch, getState);
            return super.enterScreen()(dispatch, getState);
        };
    }
}


export const EventActions = new EventActionsClass("event", EventApi);
