import {combineReducers} from 'redux';
import {sectionReducer} from "./section.reducer";
import {CrudState} from "./crud.reducer";
import {SportSection, TrainingGroup, Event, Address} from "../model";
import {NewSectionDto} from "../dto/section.dto";
import {NewTrainingGroupDto, TrainingGroupUpdateDto} from "../dto/training-group.dto";
import {trainingGroupReducer} from "./training-group.reducer";
import {UtilState, utilReducer} from "./util.reducer";
import {NewEventDto, EventUpdateDto} from "../dto/event.dto";
import {eventReducer} from "./event.reducer"
import {addressReducer} from "./address.reducer";
import {NewAddressDto, AddressUpdateDto} from "../dto/address.dto";

export type StoreState = {
    address: CrudState<Address, NewAddressDto, AddressUpdateDto>;
    section: CrudState<SportSection, NewSectionDto, SportSection>;
    trainingGroup: CrudState<TrainingGroup, NewTrainingGroupDto, TrainingGroupUpdateDto>;
    event: CrudState<Event, NewEventDto, EventUpdateDto>;
    util: UtilState;
};

export const rootReducer = combineReducers({
    address: addressReducer,
    section: sectionReducer,
    trainingGroup: trainingGroupReducer,
    event: eventReducer,
    util: utilReducer
});
