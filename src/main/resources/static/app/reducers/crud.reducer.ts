import {Action} from "redux";
import {WithId} from "../util/with-id";
import {CrudActionTypes, CountAndListResult} from "../actions/crud.action";

export interface CrudState<ItemType, NewItemDto, EditItemDto extends WithId> {
    visibleItems: ItemType[];

    listPage: number;
    listPageSize: number;
    totalCount: number;

    editedItem?: EditItemDto;
    newItem?: NewItemDto;
}

function makeInitialState<ItemType, NewItemDto, EditItemDto extends WithId>(
    createEmptyNewItem: () => NewItemDto
): CrudState<ItemType, NewItemDto, EditItemDto> {
    return {
        visibleItems: [],

        listPage: 1,
        listPageSize: 10,
        totalCount: 0,

        editedItem: null,
        newItem: createEmptyNewItem()
    };
}

export function makeCrudReducer<ItemType, NewItemDto, EditItemDto extends WithId>(
    domainName: string,
    createEmptyNewItem: () => NewItemDto,
    editConverter: (item: ItemType) => EditItemDto
): (
    state: CrudState<ItemType, NewItemDto, EditItemDto>,
    action: Action
) => CrudState<ItemType, NewItemDto, EditItemDto> {
    return (
        state: CrudState<ItemType, NewItemDto, EditItemDto> =
            makeInitialState<ItemType, NewItemDto, EditItemDto>(createEmptyNewItem),
        action: Action
    ): CrudState<ItemType, NewItemDto, EditItemDto> => {
        const anyAction: any = action as any;
        if (anyAction.crudDomain !== domainName)
            return state;

        switch (action.type) {
            case CrudActionTypes.CRUD_SET_LIST_ITEMS: {
                const data = <CountAndListResult<ItemType>><any>action;
                return Object.assign({}, state, {
                    visibleItems: data.items,
                    totalCount: data.totalCount,
                    listPage: data.page,
                    listPageSize: data.pageSize
                });
            }

            case CrudActionTypes.CRUD_LIST_ITEM_CLICK: {
                const data = action as any;
                return Object.assign({}, state, {
                    editedItem: editConverter(data.item)
                });
            }

            case CrudActionTypes.CRUD_ADD_FINISH: {
                return Object.assign({}, state, {
                    newItem: createEmptyNewItem()
                });
            }

            case CrudActionTypes.CRUD_ADD_FIELD_SET: {
                const data = action as any;
                return Object.assign({}, state, {
                    newItem: Object.assign({}, state.newItem, {
                        [data.field]: data.value
                    })
                });
            }

            case CrudActionTypes.CRUD_CANCEL_EDIT:
            case CrudActionTypes.CRUD_DELETE:
            case CrudActionTypes.CRUD_EDIT_FINISH: {
                return Object.assign({}, state, {
                    editedItem: null
                });
            }

            case CrudActionTypes.CRUD_EDIT_FIELD_SET: {
                const data = action as any;
                return Object.assign({}, state, {
                    editedItem: Object.assign({}, state.editedItem, {
                        [data.field]: data.value
                    })
                });
            }

            default:
                return state;
        }
    }
}