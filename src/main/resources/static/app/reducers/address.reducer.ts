import {Address} from "../model";
import {makeCrudReducer} from "./crud.reducer";
import {NewAddressDto, AddressUpdateDto} from "../dto/address.dto";

export const addressReducer =
    makeCrudReducer<Address, NewAddressDto, AddressUpdateDto>(
        "address",
        () => {
            return {};
        },
        item => {
            return {
                id: item.id,
                city: item.city,
                street: item.street,
                number: item.number,
                postalCode: item.postalCode,
                phone: item.phone
            }
        }
    );
