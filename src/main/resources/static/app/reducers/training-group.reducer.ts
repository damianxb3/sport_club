import {TrainingGroup} from "../model";
import {makeCrudReducer} from "./crud.reducer";
import {NewTrainingGroupDto, TrainingGroupUpdateDto} from "../dto/training-group.dto";

export const trainingGroupReducer =
    makeCrudReducer<TrainingGroup, NewTrainingGroupDto, TrainingGroupUpdateDto>(
        "trainingGroup",
        () => {
            return {};
        },
        item => {
            return {
                id: item.id,
                sectionId: item.section.id.id,
                monthlyFee: item.monthlyFee
            };
        }
    );
