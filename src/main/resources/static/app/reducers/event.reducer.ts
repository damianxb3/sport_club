import {Event} from "../model";
import {makeCrudReducer} from "./crud.reducer";
import {NewEventDto, EventUpdateDto} from "../dto/event.dto";

export const eventReducer =
    makeCrudReducer<Event, NewEventDto, EventUpdateDto>(
        "event",
        () => {
            return {};
        },
        item => {
            return {
                id: item.id,
                addressId: item.address.id.id,
                type: item.type,
                name: item.name,
                description: item.description,
                startDate: item.startDate,
                endDate: item.endDate
            };
        }
    );
