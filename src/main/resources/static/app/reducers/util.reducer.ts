import {SportSection, Address} from "../model";
import {Action} from "redux";
import {UtilActionTypes} from "../actions/util.action";

export interface UtilState {
    allSections: SportSection[];
    allAddresses: Address[]
}

const initialState: UtilState = {
    allSections: [],
    allAddresses: []
};

export function utilReducer(state: UtilState = initialState, action: Action): UtilState {
    switch (action.type) {
        case UtilActionTypes.UTIL_LOAD_ALL_SECTIONS: {
            const data = action as any;
            return Object.assign({}, state, {
                allSections: data.allSections
            });
        }
        case UtilActionTypes.UTIL_LOAD_ALL_ADDRESSES: {
            const data = action as any;
            return Object.assign({}, state, {
                allAddresses: data.allAddresses
            });
        }
        default:
            return state;
    }
}
