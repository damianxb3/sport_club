import {SportSection} from "../model";
import {NewSectionDto} from "../dto/section.dto";
import {makeCrudReducer} from "./crud.reducer";

export const sectionReducer =
    makeCrudReducer<SportSection, NewSectionDto, SportSection>(
        "section",
        () => {
            return {};
        },
        item => item
    );
