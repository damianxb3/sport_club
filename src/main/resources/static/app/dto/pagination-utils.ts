export interface PaginatedResponseDto<T> {
    items: T[];
    page: number;
}

export interface PaginatedRequest<CriteriaType> {
    criteria: CriteriaType;
    page: number;
    count: number;
}
