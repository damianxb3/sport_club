import {SportSectionType} from "../model";

export interface NewSectionDto {
    type?: SportSectionType;
}