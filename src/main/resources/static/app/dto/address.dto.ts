import {AddressId} from "../model";


export interface NewAddressDto {
    city?: string;
    street?: string;
    number?: string;
    postalCode?: string;
    phone?: string;
}

export interface AddressUpdateDto {
    id: AddressId;
    city: string;
    street: string;
    number: string;
    postalCode: string;
    phone: string;
}