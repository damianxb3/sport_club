import {EventType, EventId} from "../model";

export interface NewEventDto {
    addressId?: number;
    type?: EventType;
    name?: string;
    description?: string;
    startDate?: Date;
    endDate?: Date;
}

export interface EventUpdateDto {
    id: EventId;
    addressId: number;
    type: EventType;
    name: string;
    description: string;
    startDate: Date;
    endDate: Date;
}