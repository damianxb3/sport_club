import {TrainingGroupId} from "../model";

export interface NewTrainingGroupDto {
    sectionId?: number;
    monthlyFee?: number;
}

export interface TrainingGroupUpdateDto {
    id: TrainingGroupId;
    sectionId: number;
    monthlyFee: number;
}
