import * as React from "react";
import {makeVisibleCrudScreen} from "./crud.screen";
import {EventActions} from "../actions/event.action";
import {EventListViewItem} from "../components/event/event-list-view-item";
import {Event} from "../model";
import {NewEventDto, EventUpdateDto} from "../dto/event.dto";
import {SelectEventType} from "../components/event/select-event-type";
import {CrudEditFieldProps} from "../components/crud/crud-edit";
import {makeAddressSelect} from "../components/address/address-select";
import {makeSelectText} from "../components/select-text";
import {makeSelectDate} from "../components/select-date";
import moment = require("../../node_modules/@types/react-datepicker/node_modules/moment/moment");


function eventValidateNew(item: NewEventDto) {
    return item.addressId !== undefined &&
        item.type !== undefined &&
        item.name !== undefined &&
        item.description !== undefined &&
        item.startDate !== undefined &&
        item.endDate !== undefined;
}

function eventValidateUpdate(item: EventUpdateDto) {
    return item.id !== undefined &&
        item.addressId !== undefined &&
        item.type !== undefined &&
        item.name !== undefined &&
        item.description !== undefined &&
        item.startDate !== undefined &&
        item.endDate !== undefined;
}

const AddressSelect = makeAddressSelect();
const eventAddressIdField =
    ({state, item, handleFieldChange}: CrudEditFieldProps<NewEventDto|EventUpdateDto>) => {
        return <AddressSelect
            allAddresses={state.util.allAddresses}
            selected={item.addressId}
            onSelect={item => handleFieldChange("addressId", item)}
        />
    };

const eventTypeField = ({item, handleFieldChange}: CrudEditFieldProps<NewEventDto|EventUpdateDto>) => {
    return <SelectEventType
        type={item.type}
        handleTypeChange={type => handleFieldChange("type", type)}
    />
};

const SelectName = makeSelectText("Wprowadź nazwę wydarzenia");
const eventNameField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewEventDto|EventUpdateDto>) => {
        return <SelectName
            value={item.name}
            handleChange={newValue => handleFieldChange("name", newValue)}
        />
    };

const SelectDescription = makeSelectText("Wprowadź opis wydarzenia");
const eventDescriptionField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewEventDto|EventUpdateDto>) => {
        return <SelectDescription
            value={item.description}
            handleChange={newValue => handleFieldChange("description", newValue)}
        />
    };


const SelectStartDate = makeSelectDate("Wprowadź datę rozpoczęcia wydarzenia: ");
const eventStartDateField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewEventDto|EventUpdateDto>) => {
        return <SelectStartDate
            value={item.startDate}
            handleChange={newValue => handleFieldChange("startDate", newValue)}
        />
    };

const SelectEndDate = makeSelectDate("Wprowadź datę zakończenia wydarzenia: ");
const eventEndDateField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewEventDto|EventUpdateDto>) => {
        return <SelectEndDate
            value={item.endDate}
            handleChange={newValue => handleFieldChange("endDate", newValue)}
        />
    };

export const EventsScreen = makeVisibleCrudScreen<Event, NewEventDto, EventUpdateDto>(
    "event",
    EventActions,
    ["Id", "Adres", "Typ", "Nazwa", "Opis", "Data rozpoczęcia", "Data zakończenia"],
    EventListViewItem,
    eventValidateNew,
    eventValidateUpdate,
    [
        eventAddressIdField,
        eventTypeField,
        eventNameField,
        eventDescriptionField,
        eventStartDateField,
        eventEndDateField
    ],
    [
        eventAddressIdField,
        eventTypeField,
        eventNameField,
        eventDescriptionField,
        eventStartDateField,
        eventEndDateField
    ],
    "Lista wydarzeń",
    "Dodawanie nowego wydarzenia",
    "Edycja wydarzenia"
);