import * as React from "react";
import {makeVisibleCrudScreen} from "./crud.screen";
import {TrainingGroup} from "../model";
import {CrudEditFieldProps} from "../components/crud/crud-edit";
import {NewTrainingGroupDto, TrainingGroupUpdateDto} from "../dto/training-group.dto";
import {TrainingGroupActions} from "../actions/training-group.action";
import {TrainingGroupListViewItem} from "../components/training-group/training-group-list-view-item";
import {makeSectionSelect} from "../components/section/section-select";
import {makeSelectNumber} from "../components/select-number";

function trainingGroupValidateNew(item: NewTrainingGroupDto) {
    return item.monthlyFee !== undefined && item.sectionId !== undefined;
}

function trainingGroupValidateUpdate(item: TrainingGroupUpdateDto) {
    return item.monthlyFee !== undefined;
}

const SectionSelect = makeSectionSelect();
const sectionIdField =
    ({state, item, handleFieldChange}: CrudEditFieldProps<NewTrainingGroupDto|TrainingGroupUpdateDto>) => {
        return <SectionSelect
            allSections={state.util.allSections}
            selected={item.sectionId}
            onSelect={item => handleFieldChange("sectionId", item)}
        />
    };

const SelectAmount = makeSelectNumber("Wprowadź wartość miesięcznej składki", 0);
const monthlyFeeField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewTrainingGroupDto|TrainingGroupUpdateDto>) => {
        return <SelectAmount
            value={item.monthlyFee}
            handleChange={newValue => handleFieldChange("monthlyFee", newValue)}
        />
    };

export const GroupsScreen = makeVisibleCrudScreen<TrainingGroup, NewTrainingGroupDto, TrainingGroupUpdateDto>(
    "trainingGroup",
    TrainingGroupActions,
    ["Id", "Sekcja", "Składka miesięczna"],
    TrainingGroupListViewItem,
    trainingGroupValidateNew,
    trainingGroupValidateUpdate,
    [
        sectionIdField,
        monthlyFeeField
    ],
    [
        sectionIdField,
        monthlyFeeField
    ],
    "Lista grup treningowych",
    "Dodawanie nowej grupy treningowej",
    "Edycja grupy treningowej"
);
