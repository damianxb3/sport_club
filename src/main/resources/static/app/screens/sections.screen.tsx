import * as React from "react";
import {makeVisibleCrudScreen} from "./crud.screen";
import {SectionActions} from "../actions/section.action";
import {SectionListViewItem} from "../components/section/section-list-view-item";
import {SportSection} from "../model";
import {NewSectionDto} from "../dto/section.dto";
import {SelectSportSectionType} from "../components/section/select-sport-section-type";
import {CrudEditFieldProps} from "../components/crud/crud-edit";

function sectionValidate(item: NewSectionDto | SportSection) {
    return item.type !== undefined;
}

const sectionTypeField = ({item, handleFieldChange}: CrudEditFieldProps<NewSectionDto|SportSection>) => {
    return <SelectSportSectionType
        type={item.type}
        handleTypeChange={type => handleFieldChange("type", type)}
    />
};

export const VisibleSectionScreen = makeVisibleCrudScreen<SportSection, NewSectionDto, SportSection>(
    "section",
    SectionActions,
    ["Id", "Typ"],
    SectionListViewItem,
    sectionValidate,
    sectionValidate,
    [
        sectionTypeField
    ],
    [
        sectionTypeField
    ],
    "Lista sekcji",
    "Dodawanie nowej sekcji",
    "Edycja sekcji"
);