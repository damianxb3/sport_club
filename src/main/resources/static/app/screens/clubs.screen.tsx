import * as React from "react";
import {makeVisibleCrudScreen} from "./crud.screen";
import {NewAddressDto, AddressUpdateDto} from "../dto/address.dto";
import {AddressActions} from "../actions/address.action";
import {Address} from "../model";
import {AddressListViewItem} from "../components/address/address-list-view-item";
import {makeSelectText} from "../components/select-text";
import {CrudEditFieldProps} from "../components/crud/crud-edit";

function addressValidateNew(item: NewAddressDto) {
    return item.city !== undefined && item.street!== undefined && item.number !== undefined &&
        item.postalCode !== undefined && item.phone !== undefined;
}

function addressValidateUpdate(item: AddressUpdateDto) {
    return item.id !== undefined && item.city !== undefined && item.street!== undefined &&
        item.number !== undefined && item.postalCode !== undefined && item.phone !== undefined;
}

const SelectCity = makeSelectText("Wprowadź miasto");
const cityField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewAddressDto|AddressUpdateDto>) => {
        return <SelectCity
            value={item.city}
            handleChange={newValue => handleFieldChange("city", newValue)}
        />
    };

const SelectStreet = makeSelectText("Wprowadź ulicę");
const streetField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewAddressDto|AddressUpdateDto>) => {
        return <SelectStreet
            value={item.street}
            handleChange={newValue => handleFieldChange("street", newValue)}
        />
    };

const SelectNumber = makeSelectText("Wprowadź numer budynku");
const numberField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewAddressDto|AddressUpdateDto>) => {
        return <SelectNumber
            value={item.number}
            handleChange={newValue => handleFieldChange("number", newValue)}
        />
    };

const SelectPostalCode = makeSelectText("Wprowadź kod pocztowy");
const postalCodeField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewAddressDto|AddressUpdateDto>) => {
        return <SelectPostalCode
            value={item.postalCode}
            handleChange={newValue => handleFieldChange("postalCode", newValue)}
        />
    };

const SelectPhone = makeSelectText("Wprowadź numer telefonu");
const phoneField =
    ({item, handleFieldChange}: CrudEditFieldProps<NewAddressDto|AddressUpdateDto>) => {
        return <SelectPhone
            value={item.phone}
            handleChange={newValue => handleFieldChange("phone", newValue)}
        />
    };

export const ClubsScreen = makeVisibleCrudScreen<Address, NewAddressDto, AddressUpdateDto>(
    "address",
    AddressActions,
    ["Id", "Miasto", "Ulica", "Numer budynku", "Kod pocztowy", "Telefon"],
    AddressListViewItem,
    addressValidateNew,
    addressValidateUpdate,
    [
        cityField,
        streetField,
        numberField,
        postalCodeField,
        phoneField
    ],
    [
        cityField,
        streetField,
        numberField,
        postalCodeField,
        phoneField
    ],
    "Lista klubów",
    "Dodawanie nowego klubu",
    "Edycja klubu"
);