import * as React from "react";
import {connect} from "react-redux";
import {StoreState} from "../reducers/root.reducer";
import {CrudState} from "../reducers/crud.reducer";
import {WithId} from "../util/with-id";
import {makeCrudListView} from "../components/crud/crud-list-view";
import {CrudActions} from "../actions/crud.action";
import {ListViewItem} from "../components/list-view-item";
import {makeVisibleCrudEdit, CrudEditFieldProps} from "../components/crud/crud-edit";
import {makeVisibleCrudAdd} from "../components/crud/crud-add";

interface CrudScreenProps<ItemType, NewItemType, EditItemType extends WithId> {
    state: CrudState<ItemType, NewItemType, EditItemType>;
}

function makeCrudScreen<ItemType extends WithId, NewItemType, EditItemType extends WithId>(
    domainName: string,
    actions: CrudActions<ItemType, NewItemType, EditItemType>,
    columns: string[],
    itemPresenter: new() => ListViewItem<ItemType>,
    validateAdd: (newItem: NewItemType) => boolean,
    validateEdit: (editItem: EditItemType) => boolean,
    addFields: React.StatelessComponent<CrudEditFieldProps<EditItemType>>[],
    editFields: React.StatelessComponent<CrudEditFieldProps<EditItemType>>[],
    title: string,
    titleAdd: string,
    titleEdit: string
) {
    const ListView = makeCrudListView(domainName, actions, columns, itemPresenter);
    const ItemEdit = makeVisibleCrudEdit(domainName, actions, editFields, validateEdit);
    const ItemAdd = makeVisibleCrudAdd(domainName, actions, addFields, validateAdd);

    return ({state}: CrudScreenProps<ItemType, NewItemType, EditItemType>) => {
        return <div className="row">
            <div className="col-sm-6">
                <div>
                    <h3>{title}</h3>
                    <ListView/>
                </div>
            </div>

            <div className="col-sm-6">
                {state.editedItem ?
                    <div>
                        <h3>{titleEdit} {state.editedItem.id.id}</h3>
                        <ItemEdit/>
                    </div>
                    :
                    <div>
                        <h3>{titleAdd}</h3>
                        <ItemAdd/>
                    </div>
                }
            </div>
        </div>;
    };
}

export function makeVisibleCrudScreen<ItemType extends WithId, NewItemType, EditItemType extends WithId>(
    domainName: string,
    actions: CrudActions<ItemType, NewItemType, EditItemType>,
    columns: string[],
    itemPresenter: new() => ListViewItem<ItemType>,
    validateAdd: (newItem: NewItemType) => boolean,
    validateEdit: (editItem: EditItemType) => boolean,
    addFields: React.StatelessComponent<CrudEditFieldProps<EditItemType>>[],
    editFields: React.StatelessComponent<CrudEditFieldProps<EditItemType>>[],
    title: string,
    titleAdd: string,
    titleEdit: string
) {
    return connect(
        (state: StoreState) => {
            return {
                state: (state as any)[domainName]
            };
        }
    )(makeCrudScreen(
        domainName,
        actions,
        columns,
        itemPresenter,
        validateAdd,
        validateEdit,
        addFields,
        editFields,
        title,
        titleAdd,
        titleEdit
    ));
}
