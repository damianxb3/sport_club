import "whatwg-fetch";
import {Address} from "../model";
import {CrudApi, CrudApiImpl} from "./crud.api";
import {NewAddressDto, AddressUpdateDto} from "../dto/address.dto"

export const AddressApi: CrudApi<Address, NewAddressDto, AddressUpdateDto> =
    new CrudApiImpl<Address, NewAddressDto, AddressUpdateDto>("address");
