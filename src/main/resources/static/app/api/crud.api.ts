export interface CrudApi<ItemType, NewItemType, EditItemType> {
    list(page: number, pageSize: number): Promise<ItemType[]>;

    count(): Promise<number>;

    get(id: number): Promise<ItemType>;

    create(newItem: NewItemType): Promise<void>;

    update(editItem: EditItemType): Promise<void>;

    remove(id: number): Promise<void>;

    listAll(): Promise<ItemType[]>;
}

export class CrudApiImpl<ItemType, NewItemType, EditItemType> implements CrudApi<ItemType, NewItemType, EditItemType> {
    constructor(
        private domainName: string
    ) {}

    list(page: number, pageSize: number): Promise<ItemType[]> {
        return fetch(`/${this.domainName}/list/${page}/${pageSize}`)
            .then(response => response.json());
    }

    count(): Promise<number> {
        return fetch(`/${this.domainName}/count`)
            .then(response => response.json());
    }

    get(id: number): Promise<ItemType> {
        return fetch(`/${this.domainName}/get/${id}`)
            .then(response => response.json());
    }

    create(newItem: NewItemType): Promise<void> {
        const data = {
            method: 'PUT',
            body: JSON.stringify(newItem),
            headers: {
                "Content-Type": "application/json"
            }
        };
        return fetch(`/${this.domainName}/create`, data)
            .then(response => {});
    }

    update(editItem: EditItemType): Promise<void> {
        const data = {
            method: 'POST',
            body: JSON.stringify(editItem),
            headers: {
                "Content-Type": "application/json"
            }
        };
        return fetch(`/${this.domainName}/update`, data)
            .then(response => {});
    }

    remove(id: number): Promise<void> {
        return fetch(`/${this.domainName}/delete/${id}`, {method: 'DELETE'})
            .then(response => {});
    }

    listAll(): Promise<ItemType[]> {
        return fetch(`/${this.domainName}/all`)
            .then(response => response.json());
    }
}