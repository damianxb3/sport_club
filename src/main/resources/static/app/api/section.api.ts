import "whatwg-fetch";
import {SportSection} from "../model";
import {NewSectionDto} from "../dto/section.dto";
import {CrudApi, CrudApiImpl} from "./crud.api";

export const SectionApi: CrudApi<SportSection, NewSectionDto, SportSection> =
    new CrudApiImpl<SportSection, NewSectionDto, SportSection>("section");
