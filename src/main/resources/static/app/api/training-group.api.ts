import "whatwg-fetch";
import {TrainingGroup} from "../model";
import {NewTrainingGroupDto, TrainingGroupUpdateDto} from "../dto/training-group.dto";
import {CrudApi, CrudApiImpl} from "./crud.api";

export const TrainingGroupApi: CrudApi<TrainingGroup, NewTrainingGroupDto, TrainingGroupUpdateDto> =
    new CrudApiImpl<TrainingGroup, NewTrainingGroupDto, TrainingGroupUpdateDto>("trainingGroup");
