import "whatwg-fetch";
import {Event} from "../model";
import {CrudApi, CrudApiImpl} from "./crud.api";
import {NewEventDto, EventUpdateDto} from "../dto/event.dto";

export const EventApi: CrudApi<Event, NewEventDto, EventUpdateDto> =
    new CrudApiImpl<Event, NewEventDto, EventUpdateDto>("event");
