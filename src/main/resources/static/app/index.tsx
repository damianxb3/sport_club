import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import {Router, Route, hashHistory} from 'react-router';
import {DevTools} from "./components/debug/devtools";

// import {thunk} from 'redux-thunk'; // Uncomment when author has published correct typings
declare const require: any;
const thunk: any = require('redux-thunk').default; // Hack

import {Navbar} from "./components/navbar";
import {Sidebar} from "./components/sidebar";
import {ClubsScreen} from "./screens/clubs.screen";
import {GroupsScreen} from "./screens/groups.screen";
import {MembersScreen} from "./screens/members.screen";
import {VisibleSectionScreen} from "./screens/sections.screen";
import {StaffScreen} from "./screens/staff.screen";
import {SummaryScreen} from "./screens/summary.screen";
import {EventsScreen} from "./screens/events.screen";

import {rootReducer} from "./reducers/root.reducer";
import {SectionActions} from "./actions/section.action";
import {TrainingGroupActions} from "./actions/training-group.action";
import {EventActions} from "./actions/event.action";
import {AddressActions} from "./actions/address.action";

class App extends React.Component<{}, undefined> {
    private store = createStore(
        rootReducer,
        compose(applyMiddleware(thunk), DevTools.instrument())
    );

    constructor(...args: any[]) {
        super(...args);

        this.store.subscribe(() => console.log(this.store.getState()));
        hashHistory.listen(location => this.getRouteChangeHandler(location.pathname)());
    }

    public render(): JSX.Element {
        return <Provider store={this.store}>
            <div>
                <DevTools/>
                <Navbar/>
                <div className="row">
                    <Sidebar urlHashPart={window.location.hash}/>

                    <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                        {this.props.children}
                    </div>
                </div>
            </div>
        </Provider>;
    }

    private getRouteChangeHandler(route: string) {
        const handler = this.routeChangeHandlers[route];
        return handler ? handler : () => {};
    }

    private readonly routeChangeHandlers: {[key:string]: () => any} = {
        ["/clubs"]: () => this.store.dispatch(AddressActions.enterScreen()),
        ["/sections"]: () => this.store.dispatch(SectionActions.enterScreen()),
        ["/groups"]: () => this.store.dispatch(TrainingGroupActions.enterScreen()),
        ["/events"]: () => this.store.dispatch(EventActions.enterScreen())
    };
}

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <Route path="summary" component={SummaryScreen}/>
            <Route path="sections" component={VisibleSectionScreen}/>
            <Route path="clubs" component={ClubsScreen}/>
            <Route path="groups" component={GroupsScreen}/>
            <Route path="members" component={MembersScreen}/>
            <Route path="staff" component={StaffScreen}/>
            <Route path="events" component={EventsScreen}/>
        </Route>
    </Router> ,
    document.getElementById("example")
);