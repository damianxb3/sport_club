INSERT INTO public.club_localization (ads_id, ads_city, ads_street, ads_number, ads_postal_code, ads_phone) VALUES (7, 'Warszawa', 'Nowowiejska', '12', '00665', '500100200');
INSERT INTO public.club_localization (ads_id, ads_city, ads_street, ads_number, ads_postal_code, ads_phone) VALUES (6, 'Warszawa', 'Noakowskiego', '17', '00665', '000882277');
INSERT INTO public.club_localization (ads_id, ads_city, ads_street, ads_number, ads_postal_code, ads_phone) VALUES (8, 'Kraków', 'Rakowicka', '14', '31510', '600899899');
INSERT INTO public.club_localization (ads_id, ads_city, ads_street, ads_number, ads_postal_code, ads_phone) VALUES (9, 'Białystok', 'Młynowa', '29', '15404', '400211344');
INSERT INTO public.club_localization (ads_id, ads_city, ads_street, ads_number, ads_postal_code, ads_phone) VALUES (1, 'Gdańsk', 'Nowe Ogrody', '20/12', '80803', '407155455');

INSERT INTO public.event (evt_id, evt_address_id, evt_type, evt_name, evt_description, evt_start_date, evt_end_date) VALUES (9, 6, 'Camp', 'Obóz treningowy dla gości z Gwatemali', 'W czasie obozu członkowie polskiego klubu spotkają się z członkami ztworzyszonego klubu z Gwatemali.', '2017-02-16 19:02:55.000000', '2017-02-28 19:02:59.000000');
INSERT INTO public.event (evt_id, evt_address_id, evt_type, evt_name, evt_description, evt_start_date, evt_end_date) VALUES (10, 7, 'Gathering', 'Zgromadzenie II', 'Coroczne zgromadzenie członków klubów', '2017-03-30 19:04:24.000000', '2017-03-31 19:04:30.000000');

INSERT INTO public.sport_section (sps_id, sps_type) VALUES (5, 'football');
INSERT INTO public.sport_section (sps_id, sps_type) VALUES (6, 'archery');
INSERT INTO public.sport_section (sps_id, sps_type) VALUES (7, 'canoeing');
INSERT INTO public.sport_section (sps_id, sps_type) VALUES (8, 'volleyball');
INSERT INTO public.sport_section (sps_id, sps_type) VALUES (9, 'basketball');
INSERT INTO public.sport_section (sps_id, sps_type) VALUES (10, 'tennis');

INSERT INTO public.training_group (trg_id, trg_sport_section_id, trg_monthly_fee) VALUES (4, 5, 200.00);
INSERT INTO public.training_group (trg_id, trg_sport_section_id, trg_monthly_fee) VALUES (5, 7, 60.00);
INSERT INTO public.training_group (trg_id, trg_sport_section_id, trg_monthly_fee) VALUES (6, 10, 80.00);
INSERT INTO public.training_group (trg_id, trg_sport_section_id, trg_monthly_fee) VALUES (7, 8, 15.00);
INSERT INTO public.training_group (trg_id, trg_sport_section_id, trg_monthly_fee) VALUES (8, 8, 40.00);