ALTER TABLE training_group
	ALTER trg_monthly_fee TYPE numeric(5, 2);
ALTER TABLE training_group
	ALTER trg_monthly_fee SET NOT NULL;
	
ALTER TABLE payment_history
	ALTER pah_amount TYPE numeric(5, 2);
ALTER TABLE payment_history
	ALTER pah_amount SET NOT NULL;
	
ALTER TABLE event
	ALTER evt_address_id DROP NOT NULL;

ALTER TABLE staff
	ALTER stf_username DROP NOT NULL;
	
	
ALTER TABLE training_date
	DROP trd_training_time;
ALTER TABLE training_date
	ADD trd_start_date timestamp without time zone not null;
ALTER TABLE training_date
	ADD trd_end_date timestamp without time zone not null;
	
DROP TABLE event_assignment;

CREATE TABLE group_member_event_assignment
(
  gea_id                serial primary key,
  gea_event_id          integer not null references event (evt_id),
  gea_group_member_id   integer not null references group_member (grm_id),
  gea_active            boolean not null default true
);

CREATE TABLE staff_event_assignment
(
  sea_id                serial primary key,
  sea_event_id          integer not null references event (evt_id),
  sea_staff_id  		integer not null references staff (stf_id),
  sea_active            boolean not null default true
);