create table club_localization
(
  ads_id          serial primary key,
  ads_city        varchar(30) not null,
  ads_street      varchar(30) not null,
  ads_number      varchar(10) not null,
  ads_postal_code char(5),
  ads_phone       varchar(20) not null,

  unique (ads_city, ads_street, ads_number, ads_postal_code),
  unique (ads_phone)
);

create table event
(
  evt_id          serial primary key,
  evt_address_id  integer                     not null references club_localization (ads_id),
  evt_type        varchar(20)                 not null,
  evt_name        varchar(255)                not null,
  evt_description text,
  evt_start_date  timestamp without time zone not null,
  evt_end_date    timestamp without time zone not null
);

create table sport_section
(
  sps_id   serial primary key,
  sps_type varchar(20) not null
);

create table training_group
(
  trg_id               serial primary key,
  trg_sport_section_id integer       not null references sport_section (sps_id),
  trg_monthly_fee      numeric(3, 2) not null
);

create table training_date
(
  trd_id                serial primary key,
  trd_training_group_id integer     not null references training_group (trg_id),
  trd_training_time     varchar(30) not null
);

create table group_property
(
  grp_id                serial primary key,
  grp_training_group_id integer references training_group (trg_id),
  grp_name              varchar(45) not null,
  grp_amount            integer
);

create table staff
(
  stf_id                serial primary key,
  stf_ads_id            integer     not null references club_localization (ads_id),
  stf_training_group_id integer references training_group (trg_id),
  stf_first_name        varchar(45) not null,
  stf_last_name         varchar(45) not null,
  stf_email             varchar(50),
  stf_active            boolean     not null default true,
  stf_job_type          varchar(20),
  stf_username          varchar(50) not null,
  stf_password          varchar(50),

  unique(stf_username),
  unique(stf_email)
);

create table group_member
(
  grm_id                serial primary key,
  grm_training_group_id integer     not null references training_group (trg_id),
  grm_first_name        varchar(45) not null,
  grm_last_name         varchar(45) not null,
  grm_email             varchar(50),
  grm_active            boolean     not null default true,

  unique (grm_email)
);

create table payment_history
(
  pah_id              serial primary key,
  pah_group_member_id integer not null references group_member (grm_id),
  pah_amount          numeric(3, 2),
  pah_payment_date    integer not null
);

create table trophy
(
  try_id              serial primary key,
  try_event_id        integer references event (evt_id),
  try_group_member_id integer references group_member (grm_id),
  try_name            varchar(45) not null,
  try_type            varchar(20),
  try_description     text,
  try_acquire_date    timestamp with time zone
);

create table event_assignment
(
  eva_id                serial primary key,
  eva_event_id          integer not null references event (evt_id),
  eva_group_member_id   integer not null references group_member (grm_id),
  eva_staff_id          integer not null references staff (stf_id),
  eva_training_group_id integer not null references training_group (trg_id),
  eva_active            boolean not null default true
);

create table section_localization_list
(
  sll_address_id       integer not null references club_localization (ads_id),
  sll_sport_section_id integer not null references sport_section (sps_id),

  primary key (sll_address_id, sll_sport_section_id)
);